# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130928010032) do

  create_table "addresses", force: true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "address_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "dispatches", force: true do |t|
    t.datetime "expedicao"
    t.integer  "product_id"
    t.integer  "quantidade"
    t.string   "solicitante"
    t.string   "conferente"
    t.string   "conferente_adicional"
    t.string   "status"
    t.integer  "address_id"
  end

  add_index "dispatches", ["product_id"], name: "index_dispatches_on_product_id", using: :btree

  create_table "file_storages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "pre_receipts", force: true do |t|
    t.integer  "product_id"
    t.string   "rastreio"
    t.integer  "quantidade"
    t.datetime "fabricacao"
    t.datetime "embarque"
    t.string   "packing"
    t.string   "container",  limit: 40
    t.integer  "address_id"
    t.string   "status"
    t.string   "navio"
  end

  add_index "pre_receipts", ["address_id"], name: "index_pre_receipts_on_address_id", using: :btree
  add_index "pre_receipts", ["product_id"], name: "index_pre_receipts_on_product_id", using: :btree

  create_table "product_types", force: true do |t|
    t.string   "descricao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.integer  "provider_id"
    t.float    "peso"
    t.float    "altura"
    t.float    "largura"
    t.float    "comprimento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "codigo",          limit: 40
    t.integer  "product_type_id"
  end

  add_index "products", ["provider_id"], name: "index_products_on_provider_id", using: :btree

  create_table "providers", force: true do |t|
    t.string   "nome"
    t.string   "endereco"
    t.string   "cidade"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.string   "pais"
    t.string   "contato"
    t.string   "telefone"
  end

  create_table "receipts", force: true do |t|
    t.string   "rastreio"
    t.boolean  "avarias"
    t.float    "qtde_avariada"
    t.string   "descricao_avarias"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pre_receipt_id"
    t.string   "placa_caminhao"
    t.datetime "data_recebimento"
    t.string   "usuario"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
