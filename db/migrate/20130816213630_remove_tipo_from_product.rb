class RemoveTipoFromProduct < ActiveRecord::Migration
  	def up
  		remove_column :products, :tipo, :string
  	end    
  	def down
  		add_column :products, :tipo, :string
  	end
end
