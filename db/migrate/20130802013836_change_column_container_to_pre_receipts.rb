class ChangeColumnContainerToPreReceipts < ActiveRecord::Migration
  def change
  	change_column :pre_receipts, :container, :string, limit: 40
  end
end
