class AddColumnPreReceiptIdToReceipt < ActiveRecord::Migration
  def change
    add_column :receipts, :pre_receipt_id, :integer
  end
end
