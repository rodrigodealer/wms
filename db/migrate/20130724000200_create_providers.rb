# -*- encoding : utf-8 -*-
class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :nome
      t.string :endereco
      t.string :cidade
      t.string :estado

      t.timestamps
    end
  end
end
