class AddDataDoRecebimentoToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :data_recebimento, :timestamp
  end
end
