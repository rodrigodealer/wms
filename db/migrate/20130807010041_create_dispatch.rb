class CreateDispatch < ActiveRecord::Migration
  def change
    create_table :dispatches do |t|
    	t.timestamp :expedicao
    	t.belongs_to :product, index:true
    	t.integer :quantidade
    	t.string :solicitante
    	t.string :conferente
    	t.string :conferente_adicional
    	t.string :status
    end
  end
end
