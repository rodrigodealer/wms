class AddColumnCodigoToProduct < ActiveRecord::Migration
  def change
    add_column :products, :codigo, :string, limit: 40
  end
end
