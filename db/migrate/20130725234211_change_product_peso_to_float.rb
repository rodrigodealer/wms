# -*- encoding : utf-8 -*-
class ChangeProductPesoToFloat < ActiveRecord::Migration
  def change
    change_column :products, :peso, :float
  end
end
