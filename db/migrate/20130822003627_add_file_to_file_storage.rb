class AddFileToFileStorage < ActiveRecord::Migration
  def change
    add_attachment :file_storages, :file
  end
end
