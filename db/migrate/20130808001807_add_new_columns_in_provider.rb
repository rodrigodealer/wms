class AddNewColumnsInProvider < ActiveRecord::Migration
  def change
    add_column :providers, :email, :string
    add_column :providers, :pais, :string
    add_column :providers, :contato, :string
    add_column :providers, :telefone, :string
    remove_column :providers, :estado, :string
  end
end
