class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.string :rastreio
      t.boolean :avarias
      t.float :qtde_avariada
      t.string :descricao_avarias

      t.timestamps
    end
  end
end
