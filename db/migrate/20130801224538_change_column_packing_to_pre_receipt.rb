class ChangeColumnPackingToPreReceipt < ActiveRecord::Migration
  def change
  	change_column :pre_receipts, :packing, :string
  end
end
