# -*- encoding : utf-8 -*-
class CreatePreReceipt < ActiveRecord::Migration
  def change
    create_table :pre_receipts do |t|
    	t.belongs_to :product, index:true
    	t.string :rastreio
    	t.integer :quantidade
    	t.timestamp :fabricacao
    	t.timestamp :embarque
    	t.integer :packing
    	t.string :conhecimento_embarque
    	t.integer :container
    	t.belongs_to :address, index:true
    	t.string :status
    end
  end
end
