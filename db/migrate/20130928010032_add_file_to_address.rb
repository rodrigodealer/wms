class AddFileToAddress < ActiveRecord::Migration
  def change
    add_attachment :addresses, :file
  end
end
