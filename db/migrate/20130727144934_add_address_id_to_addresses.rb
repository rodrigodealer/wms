# -*- encoding : utf-8 -*-
class AddAddressIdToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :address_id, :integer
  end
end
