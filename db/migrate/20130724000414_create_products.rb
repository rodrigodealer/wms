# -*- encoding : utf-8 -*-
class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.belongs_to :provider, index: true
      t.string :peso
      t.string :tipo
      t.float :altura
      t.float :largura
      t.float :comprimento

      t.timestamps
    end
  end
end
