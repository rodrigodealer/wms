# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'factory_girl_rails'

user = FactoryGirl.create(:user, :email => "teste@pordotom.com.br")
user.add_role("admin")
user.save

provider = FactoryGirl.create(:provider)
FactoryGirl.create(:provider)
FactoryGirl.create(:provider)
FactoryGirl.create(:product, :provider => provider)
FactoryGirl.create(:product, :provider => provider)
FactoryGirl.create(:product, :provider => provider)
FactoryGirl.create(:product, :provider => provider)
FactoryGirl.create(:product, :provider => provider)
FactoryGirl.create(:product, :provider => provider)

address1 = FactoryGirl.create(:address)
address2 = FactoryGirl.create(:address)
FactoryGirl.create(:address)
address4 = FactoryGirl.create(:address, :address => address2)
FactoryGirl.create(:address, :address => address4)

product = FactoryGirl.create(:product)
pre_receipt1 = FactoryGirl.create(:pre_receipt,
                                  :product => product,
                                  :rastreio => "98766100",
                                  :quantidade => 6,
                                  :fabricacao => Date.new(2012, 3, 4),
                                  :embarque => Date.new(2012, 3, 10),
                                  :packing => "A",
                                  :container => "S",
                                  :address => address1,
                                  :status => "S")
                                  
pre_receipt2 = FactoryGirl.create(:pre_receipt,
                                  :product => product,
                                  :rastreio => "98766102",
                                  :quantidade => 6,
                                  :fabricacao => Date.new(2012, 3, 4),
                                  :embarque => Date.new(2012, 3, 10),
                                  :packing => "A",
                                  :container => "S",
                                  :address => address1,
                                  :status => "S")
                                  
receipt = FactoryGirl.create(:receipt, :pre_receipt => pre_receipt2)
                                  

