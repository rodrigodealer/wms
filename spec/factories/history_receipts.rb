# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
	factory :history_receipt do
		receipt { Receipt.new }
		pre_receipt { PreReceipt.new }
	end
end