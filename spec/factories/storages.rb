# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
	factory :storage do
		product { Product.new }
		receipt 10
		dispatch 0
		storage 10
		history_receipts { Array.new }
		history_dispatches { Array.new }
	end
end