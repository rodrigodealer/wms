# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :pre_receipt do
    address_id { FactoryGirl.create(:address).id }
    product_id { FactoryGirl.create(:product).id }
    fabricacao "10/03/2013"
    embarque  "10/03/2013" 
  	packing "15"
    quantidade "10"
    rastreio  "15-3556-0763"
	  container "100"
    status "Embarcado"
  end
end
