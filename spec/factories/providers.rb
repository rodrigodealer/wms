# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :provider do
    sequence(:nome) {|n| "Fornecedor #{n}" }
    endereco "Av. Santos Dumont, 4500"
    cidade "Fortaleza"
    email "teste@pordotom.com.br"
    contato "Rodrigo"
    pais "Brasil"
    telefone "32261000"
  end
end
