# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :receipt do
    rastreio "MyString"
    avarias false
    qtde_avariada 1.5
    descricao_avarias "MyString"
  end
end
