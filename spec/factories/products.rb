# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    sequence(:name) {|n| "Produto #{n}" }
    provider_id { FactoryGirl.create(:provider).id }
    sequence(:codigo) {|n| "000#{n}" } 
    product_type_id { FactoryGirl.create(:product_type).id }
    peso 1.9
    altura 1.5
    largura 1.5
    comprimento 1.5
  end
end
