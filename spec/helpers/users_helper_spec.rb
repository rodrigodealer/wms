# -*- encoding : utf-8 -*-
require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the UsersHelper. For example:
#
# describe UsersHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe UsersHelper do
  context "With one role" do
    describe "roles_to_view" do
      let(:roles) { [ Role.new(:name => "admin") ]}
      
      it "should show only one role without ','" do
        roles_to_show = helper.roles_to_view(roles)
        expect(roles_to_show).to be_eql("admin")
      end
    end
  end
  
  context "With two roles" do
    describe "roles_to_view" do
      let(:roles) { [ Role.new(:name => "admin"), Role.new(:name => "default") ]}
      
      it "should show two roles with ','" do
        roles_to_show = helper.roles_to_view(roles)
        expect(roles_to_show).to be_eql("admin, default")
      end
    end
  end
end
