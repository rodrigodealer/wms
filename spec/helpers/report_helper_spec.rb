require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the ReportHelper. For example:
#
# describe ReportHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe ReportHelper do
  describe "all css files" do
    it "should return all css files" do
      expect(helper.all_css_files).to have(3).css
    end
    
    it "should return all css content" do
      all_files = helper.all_css_files
      expect(all_css_files).to have_at_least(1).css
      expect(helper.read_content(all_css_files)).to_not be_empty
    end
    
    it "should return all css content with a tag" do
      expect(all_css_content_with_tags).to include("<style>")
    end
  end
end
