require 'spec_helper'

describe ReceiptsHelper do
  context "With receipt" do
    let(:receipt) { Receipt.new(:id => 1) }
    let(:pre_receipt) { mock_model PreReceipt }
    
    before do
      pre_receipt.should_receive(:receipt).at_least(1).and_return(receipt)
    end
    
    describe "receipt_link" do
      it "returns edit_receipt_path" do
          expect(helper.receipt_link(pre_receipt)).to be_eql(edit_receipt_path(1))
      end
    end
    
    describe "receipt_link_text" do
      it "returns 'Editar recebimento'" do
          expect(helper.receipt_link_text(pre_receipt)).to be_eql("Editar recebimento")
      end
    end
  end
  
  context "Without receipt" do
    let(:pre_receipt) { PreReceipt.new(:id => 1) }
        
    describe "receipt_link" do
      it "returns new_receipt_path" do
          expect(helper.receipt_link(pre_receipt)).to be_eql(new_receipt_path(1))
      end
    end
    
    describe "receipt_link_text" do
      it "returns 'Editar recebimento'" do
          expect(helper.receipt_link_text(pre_receipt)).to be_eql("Criar recebimento")
      end
    end
    
  end
end
