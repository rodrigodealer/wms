require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the FileStoragesHelper. For example:
#
# describe FileStoragesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe FileStoragesHelper do
  let(:date) { DateTime.new(2001,2,3,4,5,6,'+7') }
  
  describe "format_created_at" do
    it "should format date" do
      formatted_date = helper.format_created_at(date)
      expect(formatted_date).to be_eql("02/03/2001 04:05")
    end
  end
end
