# -*- encoding : utf-8 -*-
require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the AddressesHelper. For example:
#
# describe AddressesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe AddressesHelper do
  describe "address_name_increment with 2" do
    let(:address) { mock_model Address }
    before do
      address.should_receive(:nome).and_return("Endereco 1")
      address.should_receive(:parent_addresses).and_return(2)
    end
    
    it "should increment name with '**'" do
      name = helper.address_name_increment(address)
      expect(name).to be_eql("** Endereco 1")
    end
  end
  
  describe "address_name_increment with 7" do
    let(:address) { mock_model Address }
    before do
      address.should_receive(:nome).and_return("Endereco 1")
      address.should_receive(:parent_addresses).and_return(7)
    end
    
    it "should increment name with '**'" do
      name = helper.address_name_increment(address)
      expect(name).to be_eql("******* Endereco 1")
    end
  end
  
  context "address with address" do
    describe "show_if_has_address" do
      let(:address) { mock_model Address }
    
      before do
        address.should_receive(:address).twice.and_return(address)
        address.should_receive(:nome).and_return("Nome 1")
      end
    
      it "should return name" do
        expect(helper.show_if_has_address(address)).to be_eql("Nome 1")
      end
    end
  end
  
  context "address without address" do
    describe "show_if_has_address" do
      let(:address) { mock_model Address }
    
      before do
        address.should_receive(:address).once.and_return(nil)
      end
    
      it "should return name" do
        expect(helper.show_if_has_address(address)).to be_nil
      end
    end
  end
  
end
