require 'spec_helper'

feature 'Visitor signs up' do
  let(:email) { "valid@example.com" }
  let(:password) { "password" }
  before do
    FactoryGirl.create(:user, 
      :email => email, 
      :password => password, 
      :password_confirmation => password
    )
  end
  
  scenario 'with valid email and password' do
    sign_up_with(email, password)
    expect(page).to have_content('Cadastro')
  end

  scenario 'with invalid email' do
    sign_up_with('invalid_email', 'Entrar')

    expect(page).to have_content('E-mail')
    expect(page).to have_content('Senha')
  end

  scenario 'with blank password' do
    sign_up_with('valid@example.com', '')

    expect(page).to have_content('E-mail')
    expect(page).to have_content('Senha')
  end 
end