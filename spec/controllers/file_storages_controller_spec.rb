require 'spec_helper'

describe FileStoragesController do
  
  context "Guest user" do
    
    describe "GET index" do
      it "should be redirect" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "GET show" do
      it "should be redirect" do
        get :show, :id => 1
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "GET new" do
      it "should be redirect" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "should be redirect" do
        get :edit, :id => 1
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "POST create" do
      it "should be redirect" do
        post :create, :address => {}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "PUT update" do
      it "should be redirect" do
        put :update, :id => 1, :address => { "nome" => "MyString" }
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "DELETE destroy" do
      it "should be redirect" do
        delete :destroy, :id => 1
        expect(response).to redirect_to new_user_session_path
      end
    end    
  end
  
  context "Admin user" do
    let(:user) { FactoryGirl.create(:user) }
    let(:file_storage) { mock_model(FileStorage) }
    
    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "should be success" do
        get :index
        expect(response).to be_success
      end
    end
    
    describe "GET show" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "should be success" do
        FileStorage.should_receive(:find).twice.and_return(file_storage)
        get :show, :id => 1
        expect(response).to be_success
      end
    end
    
    describe "GET new" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "should be success" do
        get :new
        expect(response).to be_success
      end
    end
    
    describe "GET edit" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "should be success" do
        FileStorage.should_receive(:find).twice.and_return(file_storage)
        get :edit, :id => 1
        expect(response).to be_success
      end
    end
    
    describe "POST create" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be success" do
        post :create, :file_storage => FactoryGirl.attributes_for(:file_storage)
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "PUT update" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be success" do
        FileStorage.should_receive(:find).and_return(file_storage)
        put :update, :id => 1, :file_storage => { "nome" => "MyString" }
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be success" do
        FileStorage.should_receive(:find).and_return(file_storage)
        delete :destroy, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end    
  end
  
  context "Default user" do
    let(:user) { FactoryGirl.create(:user) }
    let(:file_storage) { mock_model(FileStorage) }
    
    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        get :index
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET show" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        FileStorage.should_receive(:find).and_return(file_storage)
        get :show, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET new" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        get :new
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        FileStorage.should_receive(:find).and_return(file_storage)
        get :edit, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST create" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        post :create, :file_storage => FactoryGirl.attributes_for(:file_storage)
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "PUT update" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        FileStorage.should_receive(:find).and_return(file_storage)
        put :update, :id => 1, :file_storage => { "nome" => "MyString" }
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should be redirect" do
        FileStorage.should_receive(:find).and_return(file_storage)
        delete :destroy, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end    
  end

end
