# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UsersController do
  context 'Admin Logged user' do
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "should return success" do
        get :index
        expect(response).to be_success
      end
    end
    
    describe "GET report" do
      before do
        user.add_role(:admin)
        user.save
        RestClient.should_receive(:post).with(any_args()).and_return("somevalue")
        sign_in user
      end
      
      it "should return success" do
        get :report, :format => :pdf
        expect(response).to be_success
      end
    end
    
    describe "GET roles" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        User.should_receive(:find).twice.and_return(User.new)
      end
      
      it "should return success" do
        get :roles, :id => 1, :format => :json
        expect(response).to be_success
      end
    end
    
    describe "POST save" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        other_user.should_receive(:save).and_return(true)
        User.should_receive(:new).and_return(other_user)
      end
      
      it "should return redirect" do
        user = { :email => "bla@gmail.com", :id => "" }
        post :save, :user => user, :roles => {}
        expect(response).to be_redirect
      end
    end
    
    describe "POST save" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        other_user.should_receive(:save).and_return(false)
        User.should_receive(:new).and_return(other_user)
      end
      
      it "should return success" do
        user = { :email => "bla@gmail.com", :id => "" }
        post :save, :user => user, :roles => {}
        expect(response).to be_success
      end
    end
    
    describe "POST save with roles" do
      let(:other_user) { mock_model User }
      let(:role) { mock_model Role }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        other_user.should_receive(:save).and_return(true)
        other_user.should_receive(:add_role)
        role.should_receive(:name).and_return("admin")
        Role.should_receive(:find).and_return(role)
        User.should_receive(:new).and_return(other_user)
      end
      
      it "should return success" do
        user = { :email => "bla@gmail.com", :id => "" }
        post :save, :user => user, :roles => ",1"
        expect(response).to be_redirect
      end
    end
    
    describe "DELETE destroy" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        User.should_receive(:find).twice.and_return(other_user)
      end
      
      it "should return success" do
        delete :destroy, :id => 1
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH save" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        other_user.should_receive(:update).and_return(true)
        User.should_receive(:find_by_email).once.and_return(other_user)
      end
      
      it "should return redirect" do
        user = { :email => "bla@gmail.com", :id => 1 }
        post :save, :user => user, :roles => {}
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH save" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        other_user.should_receive(:update).and_return(false)
        User.should_receive(:find_by_email).once.and_return(other_user)
      end
      
      it "should return success" do
        user = { :email => "bla@gmail.com", :id => 1 }
        post :save, :user => user, :roles => {}
        expect(response).to be_success
      end
    end
    
    describe "GET edit" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        User.should_receive(:find).with(any_args()).twice.and_return(other_user)
      end
      
      it "should return success" do
        get :edit, :id => 1
        expect(response).to be_success
      end
    end
    
    describe "GET new" do
      let(:other_user) { mock_model User }
      before do
        user.add_role(:admin)
        user.save
        sign_in user
        User.should_receive(:new).with(any_args()).twice.and_return(other_user)
      end
      
      it "should return success" do
        get :new
        expect(response).to be_success
      end
    end
  end
  
  context 'Guest user' do
    describe "GET index" do
      it "should return redirect" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "GET edit" do
      it "should return redirect" do
        get :edit, :id => 1
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "GET report" do
      it "should return redirect" do
        get :report, :format => :pdf
        expect(response.code).to be_eql("401")
      end
    end
    
    describe "GET roles" do
      it "should return redirect" do
        get :roles, :id => 1, :format => :json
        expect(response.code).to be_eql("401")
      end
    end
    
    describe "POST save" do
      it "should return redirect" do
        post :save
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "PATCH save" do
      it "should return redirect" do
        patch :save
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "DELETE destroy" do
      it "should return redirect" do
        delete :destroy, :id => 1
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "GET new" do
      it "should return redirect" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
  
  context 'Regular Logged user' do
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should return permission_denied" do
        get :index
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET roles" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
        User.should_receive(:find).with(any_args()).once.and_return(other_user)
      end
      
      it "should return permission_denied" do
        get :roles, :id => 1, :format => :json
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET report" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should return permission_denied" do
        get :report, :format => :pdf
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET edit" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
        User.should_receive(:find).with(any_args()).once.and_return(other_user)
      end
      
      it "should return permission_denied" do
        get :edit, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "GET new" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
        User.should_receive(:new).with(any_args()).once.and_return(other_user)
      end
      
      it "should return permission_denied" do
        get :new
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "POST save" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should return permission_denied" do
        post :save
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "DELETE destroy" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
        User.should_receive(:find).with(any_args()).once.and_return(other_user)
      end
      
      it "should return permission_denied" do
        delete :destroy, :id => 1
        expect(response).to redirect_to(permission_denied_path)
      end
    end
    
    describe "PATCH save" do
      let(:other_user) { mock_model User }
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "should return permission_denied" do
        patch :save
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  end
end
