# -*- encoding : utf-8 -*-
require 'spec_helper'

describe AddressesController do
  context 'Admin Logged user' do
    let(:valid_attributes) { { "nome" => "MyString" } }
    let(:valid_session) { {} }
    let(:address) { mock_model(Address) }
    let(:user) { FactoryGirl.create(:user) }

    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns all addresses as @addresses" do
        get :index
        expect(response).to be_success
      end
    end

    describe "GET show" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns the requested address as @address" do
        Address.should_receive(:find).twice.and_return(address)
        get :show, :id => 1
        expect(response).to be_success
      end
    end

    describe "GET new" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns a new address as @address" do
        get :new
        expect(response).to be_success
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns the requested address as @address" do
        Address.should_receive(:find).twice.and_return(address)
        get :edit, :id => 1
        expect(response).to be_success
      end
    end

    describe "POST create" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      describe "with valid params" do
        it "creates a new Address" do
          expect {
            post :create, {:address => valid_attributes}, valid_session
          }.to change(Address, :count).by(1)
        end

        it "assigns a newly created address as @address" do
          post :create, {:address => valid_attributes}, valid_session
          assigns(:address).should be_a(Address)
          assigns(:address).should be_persisted
        end

        it "redirects to the created address" do
          post :create, {:address => valid_attributes}, valid_session
          response.should redirect_to(Address.last)
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved address as @address" do
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          assigns(:address).should be_a_new(Address)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      describe "with valid params" do
        it "updates the requested address" do
          address.should_receive(:update).with({ "nome" => "MyString" }).and_return(true)
          Address.should_receive(:find).twice.and_return(address)
          put :update, {:id => address.to_param, :address => { "nome" => "MyString" }}, valid_session
        end

        it "assigns the requested address as @address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          assigns(:address).should eq(address)
        end

        it "redirects to the address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          response.should redirect_to(address)
        end
      end

      describe "with invalid params" do
        it "assigns the address as @address" do
          address = Address.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          assigns(:address).should eq(address)
        end

        it "re-renders the 'edit' template" do
          address = Address.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          response.should render_template("edit")
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "destroys the requested address" do
        address = Address.create! valid_attributes
        expect {
          delete :destroy, {:id => address.to_param}, valid_session
        }.to change(Address, :count).by(-1)
      end

      it "redirects to the addresses list" do
        address = Address.create! valid_attributes
        delete :destroy, {:id => address.to_param}, valid_session
        response.should redirect_to(addresses_url)
      end
    end
  end
  
  context 'Regular Logged user' do
    let(:valid_attributes) { { "nome" => "MyString" } }
    let(:valid_session) { {} }
    let(:user) { FactoryGirl.create(:user) }

    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns all addresses as @addresses" do
        address = Address.create! valid_attributes
        get :index, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET show" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns the requested address as @address" do
        address = Address.create! valid_attributes
        get :show, {:id => address.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET new" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns a new address as @address" do
        get :new, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns the requested address as @address" do
        address = Address.create! valid_attributes
        get :edit, {:id => address.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST create" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      describe "with valid params" do
        it "creates a new Address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns a newly created address as @address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the created address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved address as @address" do
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "PUT update" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      describe "with valid params" do
        it "updates the requested address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => { "nome" => "MyString" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns the requested address as @address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        it "assigns the address as @address" do
          address = Address.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'edit' template" do
          address = Address.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "destroys the requested address" do
        address = Address.create! valid_attributes
        delete :destroy, {:id => address.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end

      it "redirects to the addresses list" do
        address = Address.create! valid_attributes
        delete :destroy, {:id => address.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  end
  
  context 'Guest user' do
    let(:valid_attributes) { { "nome" => "MyString" } }
    let(:valid_session) { {} }

    describe "GET index" do
      it "assigns all addresses as @addresses" do
        address = Address.create! valid_attributes
        get :index, {}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET show" do      
      it "assigns the requested address as @address" do
        address = Address.create! valid_attributes
        get :show, {:id => address.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET new" do
      it "assigns a new address as @address" do
        get :new, {}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "assigns the requested address as @address" do
        address = Address.create! valid_attributes
        get :edit, {:id => address.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "creates a new Address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "assigns a newly created address as @address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "redirects to the created address" do
          post :create, {:address => valid_attributes}, valid_session
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved address as @address" do
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "re-renders the 'new' template" do
          Address.any_instance.stub(:save).and_return(false)
          post :create, {:address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => { "nome" => "MyString" }}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "assigns the requested address as @address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "redirects to the address" do
          address = Address.create! valid_attributes
          put :update, {:id => address.to_param, :address => valid_attributes}, valid_session
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "assigns the address as @address" do
          address = Address.create! valid_attributes
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to new_user_session_path
        end

        it "re-renders the 'edit' template" do
          address = Address.create! valid_attributes
          Address.any_instance.stub(:save).and_return(false)
          put :update, {:id => address.to_param, :address => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "DELETE destroy" do
      it "destroys the requested address" do
        address = Address.create! valid_attributes
        delete :destroy, {:id => address.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end

      it "redirects to the addresses list" do
        address = Address.create! valid_attributes
        delete :destroy, {:id => address.to_param}, valid_session
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
