# -*- encoding : utf-8 -*-
require 'spec_helper'

describe DispatchesController do	
  context "Admin is logged" do
    let(:unprocessable_entity) {422}
    let(:valid_address) {{id: 1, nome: 'Endereco de teste'}}
    let(:valid_attributes) {{:id => "1", :expedicao => "10/08/2013", :quantidade => "10", :solicitante => "Pedro",
    	:conferente => "Ronaldo maciel", :conferente_adicional => "Rodrigo", :status => "Saida",
    	:product_id => 1
    }}
    let(:product_valid_attributes) {{:id => 1, :name => "MyString", 
    		:codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1) }
    }
    let(:valid_session) { }
  	let(:user) { FactoryGirl.create(:user)}
    let(:valid_pre_receipt) {{:id => 1, :product_id => 1, :quantidade => 20, :embarque => "08/08/2013", 
        :rastreio => "1", :fabricacao => "06/08/2013", :packing => "10", 
        :container => "AC", :status => "Embarcado", :address => Address.new(:id => 1)}}
  let(:valid_receipt) {{:pre_receipt => PreReceipt.new(valid_pre_receipt), :rastreio => "10", 
                        :avarias => "1", :qtde_avariada => "2", :data_recebimento => "08/08/2013"}}

  	describe "GET index" do
  		before do
  			user.add_role(:admin)
  	    user.save
        sign_in user
  		end

  		it "assigns all dispatches as @dispatches" do
  			dispatch = Dispatch.create! valid_attributes
  			get :index, {}, valid_session
  			assigns(:dispatches).should eq([dispatch])
  		end
  	end

  	describe "GET new" do
      before do
      	user.add_role(:admin)
        user.save
        sign_in user
        Address.create! valid_address       
        @addresses = Address.all.to_set.classify {
          |address| address.address
        }
  		end

  		it "assigns a new dispatch as @dispatch" do    			
  			get :new, {}, valid_session
  			assigns(:dispatch).should be_a_new(Dispatch)
  		end

  		it "assigns all products as @products" do
  			product = Product.create! product_valid_attributes
  			get :new, {}, valid_session
  			assigns(:products).should eq([product])
  		end

      it "assigns all addresses as @addresses" do
        get :new, {}, valid_session        
        assigns(:addresses).should eq(@addresses)
      end
  	end

  	describe "GET edit" do
  		before do
  			user.add_role(:admin)
  	    user.save
  			sign_in user
  			@dispatch = Dispatch.create! valid_attributes
        Address.create! valid_address       
        @addresses = Address.all.to_set.classify {
          |address| address.address
        }
  		end

  		it "assigns the requested dispatch as @dispatch" do    			
  			get :edit, {:id => @dispatch.to_param}, valid_session
  			assigns(:dispatch).should eq(@dispatch)
  		end

  		it "assigns all products as @products" do
  			product = Product.create! product_valid_attributes
  			get :edit, {:id => @dispatch.to_param}, valid_session
  			assigns(:products).should eq([product])
  		end
      it "assigns all addresses as @addresses" do
        get :edit, {:id => @dispatch.to_param}, valid_session        
        assigns(:addresses).should eq(@addresses)
      end
  	end

  	describe "POST create" do
  		describe "with valid attributes" do
        before do
    			user.add_role(:admin)
  		    user.save
  			  sign_in user
          product = Product.create! product_valid_attributes
          receipt = Receipt.create! valid_receipt
    		end
      
    		it "creates a new dispatch" do
    			expect {
    				post :create, {:dispatch => valid_attributes, :format => :js}, valid_session
    			}.to change(Dispatch, :count).by(1)
    		end
      
    		it "serializes as json the new dispatch" do
    			dispatch = Dispatch.new valid_attributes
    			post :create, {:dispatch => valid_attributes, :format => :js}, valid_session	    			
    			expected = dispatch.to_json(:include => { :product => {:only => :name} } )
    			expect(response.body).to eq(expected)	    			
    		end	    	
  		end
    
  		describe "with invalid attributes" do
  			before do
    			user.add_role(:admin)
  		    user.save
          sign_in user					          
    		end
      
    		it "responses error status" do
  				Dispatch.any_instance.stub(:saida!).and_return(false)
  				post :create, {:dispatch => {:solicitante => "Carlos"} , :format => :js}, valid_session			
  				expect(response.status).to eq(unprocessable_entity)
  			end
  		end

      describe "product has not enough storage" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
          Receipt.destroy_all
        end
        
        it "responses error message" do          
          post :create, {:dispatch => {:expedicao => "10/08/2013", :solicitante => "Carlos"} , :format => :js}, valid_session     
          expected = {error: "produto nao disponivel em estoque neste endereco."}
          expect(response.body).to eq(expected.to_json)  
        end
      end
  	end
  	describe "PUT update" do
      describe "with success" do
    		before do
    			user.add_role(:admin)
  		    user.save
  			  sign_in user				
    		end
    
    		it "returns http success" do
  				dispatch = Dispatch.create! valid_attributes				
  				put :update, :id => dispatch.to_param, :dispatch => valid_attributes
  				expect(response).to be_redirect
  			end
      end
    
      describe "with success" do
    		before do
    			user.add_role(:admin)
  		    user.save
  			  sign_in user			
          dispatch = mock_model(Dispatch)
          dispatch.should_receive(:update).and_return(false)
          Dispatch.should_receive(:find).twice.and_return(dispatch)	
    		end
    
    		it "returns http success" do
  				saved_dispatch = Dispatch.create!(valid_attributes)
  				put :update, :id => saved_dispatch.to_param, :dispatch => valid_attributes
  				expect(response).to be_success
  			end
      end	
    end
  end
  
  context "Default is logged" do
    let(:unprocessable_entity) {422}
    let(:valid_attributes) {{:id => "1", :expedicao => "10/08/2013", :quantidade => "10", :solicitante => "Pedro",
    	:conferente => "Ronaldo maciel", :conferente_adicional => "Rodrigo", :status => "Saida",
    	:product_id => 1
    }}
    let(:product_valid_attributes) {{:id => 1, :name => "MyString", 
    		:codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1) }
    }
    let(:valid_session) { }
  	let(:user) { FactoryGirl.create(:user)}

  	describe "GET index" do
  		before do
  			user.add_role(:default)
  	    user.save
        sign_in user
  		end

  		it "assigns all dispatches as @dispatches" do
  			dispatch = Dispatch.create! valid_attributes
  			get :index, {}, valid_session
  			expect(response).to redirect_to(permission_denied_path)
  		end
  	end

  	describe "GET new" do
      before do
      	user.add_role(:default)
        user.save
        sign_in user
  		end

  		it "assigns a new dispatch as @dispatch" do    			
  			get :new, {}, valid_session
  			expect(response).to redirect_to(permission_denied_path)
  		end

  		it "assigns all products as @products" do
  			product = Product.create! product_valid_attributes
  			get :new, {}, valid_session
  			expect(response).to redirect_to(permission_denied_path)
  		end
  	end

  	describe "GET edit" do
  		before do
  			user.add_role(:default)
  	    user.save
  			sign_in user
  			@dispatch = Dispatch.create! valid_attributes
  		end

  		it "assigns the requested dispatch as @dispatch" do    			
  			get :edit, {:id => @dispatch.to_param}, valid_session
  			expect(response).to redirect_to(permission_denied_path)
  		end

  		it "assigns all products as @products" do
  			product = Product.create! product_valid_attributes
  			get :edit, {:id => @dispatch.to_param}, valid_session
  			expect(response).to redirect_to(permission_denied_path)
  		end
  	end

  	describe "POST create" do
  		describe "with valid attributes" do
        before do
    			user.add_role(:default)
  		    user.save
  			  sign_in user
          product = Product.create! product_valid_attributes		
    		end
      
    		it "creates a new dispatch" do
    			post :create, {:dispatch => valid_attributes, :format => :js}, valid_session
    			expect(response).to redirect_to(permission_denied_path)
    		end
      
    		it "serializes as json the new dispatch" do
    			dispatch = Dispatch.new valid_attributes
    			post :create, {:dispatch => valid_attributes, :format => :js}, valid_session	    			
    			expected = dispatch.to_json(:include => { :product => {:only => :name} } )
    			expect(response).to redirect_to(permission_denied_path)
    		end	    	
  		end
    
  		describe "with invalid attributes" do
  			before do
    			user.add_role(:default)
  		    user.save
          sign_in user					
    		end
      
    		it "responses error status" do
  				Dispatch.any_instance.stub(:save).and_return(false)
  				post :create, {:dispatch => {:solicitante => "Carlos"} , :format => :js}, valid_session			
  				expect(response).to redirect_to(permission_denied_path)
  			end
  		end
  	end
  	describe "PUT update" do
      describe "with success" do
    		before do
    			user.add_role(:default)
  		    user.save
  			  sign_in user				
    		end
    
    		it "returns http success" do
  				dispatch = Dispatch.create! valid_attributes				
  				put :update, :id => dispatch.to_param, :dispatch => valid_attributes
  				expect(response).to redirect_to(permission_denied_path)
  			end
      end
    
      describe "with success" do
    		before do
    			user.add_role(:default)
  		    user.save
  			  sign_in user			
          dispatch = mock_model(Dispatch)
          Dispatch.should_receive(:find).once.and_return(dispatch)	
    		end
    
    		it "returns http success" do
  				saved_dispatch = Dispatch.create!(valid_attributes)
  				put :update, :id => saved_dispatch.to_param, :dispatch => valid_attributes
  				expect(response).to redirect_to(permission_denied_path)
  			end
      end	
    end
  end
  
  context "Guest user" do
    describe "GET index" do      
      it "should be redirected" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET show" do      
      it "should be redirected" do
        get :show, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET new" do
      it "should be redirected" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "should be redirected" do
        get :edit, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "should be redirected" do
          post :create
          expect(response).to redirect_to new_user_session_path
        end

      end

      describe "with invalid params" do
        it "should be redirected" do
          post :create, {:provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "MyString" }}
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
