require 'spec_helper'

describe ReceiptReportsController do

  let(:user) { FactoryGirl.create(:user) }

  context 'Admin Logged user'
    describe "GET index" do
    	before do
        user.add_role(:admin)
        user.save        
        sign_in user

        @product_type = FactoryGirl.build(:product_type)
        @product = FactoryGirl.build(:product)
        @parent_address = FactoryGirl.build(:address)
		    @address = FactoryGirl.build(:address)
        @parent_address.address = @address
        ProductType.should_receive(:all).and_return([@product_type])        	
        Product.should_receive(:all).and_return([@product])        	
        Address.stub(:all).and_return([@parent_address])        	
      end
    	it "returns all product_types as @product_types" do
    		get :index, {}
    		assigns(:product_types).should eq([@product_type]) 
    	end
    	it "returns all products as @products" do
    		get :index, {}
    		assigns(:products).should eq([@product]) 
    	end
    	it "returns all addresses as @addresses" do
    		addresses = Address.all.to_set.classify {
				  |address| address.address
			  }
    		get :index, {}
    		assigns(:addresses).should eq(addresses) 
    	end
    end
    describe "GET report" do
      before do
        user.add_role(:admin)
        user.save
        RestClient.should_receive(:post).with(any_args()).and_return("somevalue")
        sign_in user
      end
      
      it "should return success" do
        get :report, :format => :pdf
        expect(response).to be_success
      end
    end
  context 'Default Logged user'    
    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        get :index, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        post :report, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  context 'Guest Logged user'    
    describe "GET index" do
      before do
        user.add_role(:guest)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        get :index, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        post :report, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end 

end
