require 'spec_helper'

describe ProductTypesController do
	context "Admin user is logged" do

		let(:user) { FactoryGirl.create(:user)}
		let(:product_type) { FactoryGirl.create(:product_type, :id => 1) }

		before do
			user.add_role(:admin)
    	user.save
    	sign_in user        
		end

		describe "GET index" do
			it "returns all ProductTypes as @product_types" do
				get :index, {}
				assigns(:product_types).should eq([product_type])
			end
		end

		describe "GET new" do
			it "returns a new ProductType as @product_type" do
				get :new, {}
				assigns(:product_type).should be_a_new(ProductType)
			end			
		end		

		describe "GET edit" do
			it "returns the selected product_type as @product_type" do
				product_type.save
				get :edit, {:id => "1"}, {}
				assigns(:product_type).should be_eql(product_type)
			end
		end

		describe "POST create" do
			describe "with valid params" do
				it "creates a new product_type" do
					expect {
						post :create, {:product_type => {:descricao => "Construindo"}}
					}.to change(ProductType, :count).by(1)
				end	
				it "redirects to product_types path" do
					post :create, {:product_type => {:descricao => "Construindo"}}
					response.should redirect_to(product_types_path)
				end
			end
			describe "with invalid params" do
				it "renders new template" do
					ProductType.any_instance.stub(:save).and_return(false)
					post :create, {:product_type => {:descricao => "Invalid"}}
					response.should render_template("new")
				end
			end
		end

		describe "PUT update" do
			before do
				product_type.save
			end
			describe "with valid params" do				
				it "redirects to product_types path" do
					put :update, {:id => "1", :product_type => {:descricao => "Construindo Alterando"}}
					response.should redirect_to(product_types_path)
				end				
			end
			describe "with invalid params" do
				it "re-renders edit template" do
					ProductType.any_instance.stub(:update).with(any_args()).and_return(false)
					put :update, {:id => "1", :product_type => {:descricao => "Invalido"}}
					response.should render_template("edit")
				end				
			end
		end

		describe "DELETE destroy" do
			before do
				product_type.save
			end
			it "removes the selected product_type" do
				expect {
					delete :destroy, {:id => "1"}
				}.to change(ProductType, :count).by(-1)
			end
			it "redirects to product_types path" do
				delete :destroy, {:id => "1"}
				response.should redirect_to(product_types_path)
			end			
		end
  end

	context "Default user is logged" do

		let(:user) { FactoryGirl.create(:user)}
		let(:product_type) { FactoryGirl.create(:product_type, :id => 1) }

		before do
			user.add_role(:default)
    	user.save
    	sign_in user        
		end

		describe "GET index" do
			it "returns all ProductTypes as @product_types" do
				get :index, {}
				expect(response).to redirect_to(permission_denied_path)
			end
		end

		describe "GET new" do
			it "returns a new ProductType as @product_type" do
				get :new, {}
				expect(response).to redirect_to(permission_denied_path)
			end			
		end		

		describe "GET edit" do
			it "returns the selected product_type as @product_type" do
				product_type.save
				get :edit, {:id => "1"}, {}
				expect(response).to redirect_to(permission_denied_path)
			end
		end

		describe "POST create" do
			describe "with valid params" do
				it "creates a new product_type" do
					post :create, {:product_type => {:descricao => "Construindo"}}
          expect(response).to redirect_to(permission_denied_path)
				end	
				it "redirects to product_types path" do
					post :create, {:product_type => {:descricao => "Construindo"}}
					expect(response).to redirect_to(permission_denied_path)
				end
			end
			describe "with invalid params" do
				it "renders new template" do
					ProductType.any_instance.stub(:save).and_return(false)
					post :create, {:product_type => {:descricao => "Invalid"}}
					expect(response).to redirect_to(permission_denied_path)
				end
			end
		end

		describe "PUT update" do
			before do
				product_type.save
			end
			describe "with valid params" do				
				it "redirects to product_types path" do
					put :update, {:id => "1", :product_type => {:descricao => "Construindo Alterando"}}
					expect(response).to redirect_to(permission_denied_path)
				end				
			end
			describe "with invalid params" do
				it "re-renders edit template" do
					ProductType.any_instance.stub(:update).with(any_args()).and_return(false)
					put :update, {:id => "1", :product_type => {:descricao => "Invalido"}}
					expect(response).to redirect_to(permission_denied_path)
				end				
			end
		end

		describe "DELETE destroy" do
			before do
				product_type.save
			end
			it "removes the selected product_type" do
				delete :destroy, {:id => "1"}
				expect(response).to redirect_to(permission_denied_path)
			end
			it "redirects to product_types path" do
				delete :destroy, {:id => "1"}
				expect(response).to redirect_to(permission_denied_path)
			end			
		end		
  end			
  
  context "Guest user" do
    describe "GET index" do      
      it "should be redirected" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET new" do
      it "should be redirected" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "should be redirected" do
        get :edit, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "should be redirected" do
          post :create
          expect(response).to redirect_to new_user_session_path
        end

      end

      describe "with invalid params" do
        it "should be redirected" do
          post :create, {:provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "MyString" }}
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "DELETE destroy" do
      it "should be redirected" do
        delete :destroy, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
