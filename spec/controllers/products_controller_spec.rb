# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ProductsController do

  context 'Admin Logged user' do
    let(:provider) { FactoryGirl.create(:provider, :id => 1) }
    let(:product_type) { {:id => "1", :descricao => "Construcao"} }
    let(:valid_attributes) { FactoryGirl.attributes_for(:product)}
    let(:valid_session) { }
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user        
      end
    
      it "assigns all products as @products" do
        product = Product.create! valid_attributes
        get :index, {}, valid_session
        assigns(:products).should eq([product])
      end
    end

    describe "GET show" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
    
      it "assigns the requested product as @product" do
        product = Product.create! valid_attributes
        get :show, {:id => product.to_param}, valid_session
        assigns(:product).should eq(product)
      end
    end

    describe "GET new" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
    
      it "assigns a new product as @product" do
        get :new, {}, valid_session
        assigns(:product).should be_a_new(Product)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
    
      it "assigns the requested product as @product" do
        product = Product.create! valid_attributes
        get :edit, {:id => product.to_param}, valid_session
        assigns(:product).should eq(product)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "creates a new Product" do
          expect {
            post :create, {:product => valid_attributes}, valid_session
          }.to change(Product, :count).by(1)
        end

        it "assigns a newly created product as @product" do
          post :create, :product => valid_attributes
          assigns(:product).should be_a(Product)
          assigns(:product).should be_persisted
        end

        it "redirects to the created product" do
          post :create, {:product => valid_attributes}, valid_session
          response.should redirect_to(Product.last)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "assigns a newly created but unsaved product as @product" do
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          post :create, {:product => { "name" => "invalid value" }}, valid_session
          assigns(:product).should be_a_new(Product)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          post :create, {:product => { "name" => "invalid value" }}, valid_session
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "updates the requested product" do
          product = Product.create! valid_attributes
          Product.any_instance.should_receive(:update).with({ "name" => "MyString" })
          put :update, {:id => product.to_param, :product => { "name" => "MyString" }}, valid_session
        end

        it "assigns the requested product as @product" do
          product = Product.create! valid_attributes
          put :update, {:id => product.to_param, :product => valid_attributes}, valid_session
          assigns(:product).should eq(product)
        end

        it "redirects to the product" do
          product = Product.create! valid_attributes
          put :update, {:id => product.to_param, :product => valid_attributes}, valid_session
          response.should redirect_to(product)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "assigns the product as @product" do
          product = Product.create! valid_attributes
          Product.any_instance.stub(:save).and_return(false)
          put :update, {:id => product.to_param, :product => { "name" => "invalid value" }}, valid_session
          assigns(:product).should eq(product)
        end

        it "re-renders the 'edit' template" do
          product = Product.create! valid_attributes
          Product.any_instance.stub(:save).and_return(false)
          put :update, {:id => product.to_param, :product => { "name" => "invalid value" }}, valid_session
          response.should render_template("edit")
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
    
      it "destroys the requested product" do
        product = Product.create! valid_attributes
        expect {
          delete :destroy, {:id => product.to_param}, valid_session
        }.to change(Product, :count).by(-1)
      end

      it "redirects to the products list" do
        product = Product.create! valid_attributes
        delete :destroy, {:id => product.to_param}, valid_session
        response.should redirect_to(products_url)
      end
    end
  end
  
  context 'Regular Logged user' do
    let(:valid_attributes) {{ :name => "MyString", :codigo => "1345", :product_type_id => "1", :provider => Provider.new(:id => 1) }}
    let(:valid_session) { }
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
    
      it "assigns all products as @products" do
        product = Product.create! valid_attributes
        get :index, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET show" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
    
      it "assigns the requested product as @product" do
        product = Product.create! valid_attributes
        get :show, {:id => product.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET new" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
    
      it "assigns a new product as @product" do
        get :new, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
    
      it "assigns the requested product as @product" do
        product = Product.create! valid_attributes
        get :edit, {:id => product.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "creates a new Product" do
          post :create, {:product => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns a newly created product as @product" do
          post :create, {:product => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the created product" do
          post :create, {:product => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "assigns a newly created but unsaved product as @product" do
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          post :create, {:product => { "name" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          post :create, {:product => { "name" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "updates the requested product" do
          product = Product.create! valid_attributes
          put :update, {:id => product.to_param, :product => { "name" => "MyString" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns the requested product as @product" do
          product = Product.create! valid_attributes
          put :update, {:id => product.to_param, :product => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the product" do
          product = Product.create! valid_attributes
          put :update, {:id => product.to_param, :product => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "assigns the product as @product" do
          product = Product.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          put :update, {:id => product.to_param, :product => { "name" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'edit' template" do
          product = Product.create! valid_attributes
          # Trigger the behavior that occurs when invalid params are submitted
          Product.any_instance.stub(:save).and_return(false)
          put :update, {:id => product.to_param, :product => { "name" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
    
      it "destroys the requested product" do
        product = Product.create! valid_attributes
        delete :destroy, {:id => product.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end

      it "redirects to the products list" do
        product = Product.create! valid_attributes
        delete :destroy, {:id => product.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  end
  
  context "Guest user" do
    describe "GET index" do      
      it "should be redirected" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET show" do      
      it "should be redirected" do
        get :show, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET new" do
      it "should be redirected" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "should be redirected" do
        get :edit, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end
    
    describe "POST create" do
      describe "with valid params" do
        it "should be redirected" do
          post :create
          expect(response).to redirect_to new_user_session_path
        end

      end

      describe "with invalid params" do
        it "should be redirected" do
          post :create, {:provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "MyString" }}
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "DELETE destroy" do
      it "should be redirected" do
        delete :destroy, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

end
