# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ProvidersController do

  context "Admin Logged user" do
    let(:valid_attributes) { FactoryGirl.attributes_for(:provider) }
    let(:valid_session) { {} }
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns all providers as @providers" do
        provider = Provider.create! valid_attributes
        get :index, {}, valid_session
        assigns(:providers).should eq([provider])
      end
    end

    describe "GET show" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns the requested provider as @provider" do
        provider = Provider.create! valid_attributes
        get :show, {:id => provider.to_param}, valid_session
        assigns(:provider).should eq(provider)
      end
    end

    describe "GET new" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns a new provider as @provider" do
        get :new, {}, valid_session
        assigns(:provider).should be_a_new(Provider)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "assigns the requested provider as @provider" do
        provider = Provider.create! valid_attributes
        get :edit, {:id => provider.to_param}, valid_session
        assigns(:provider).should eq(provider)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "creates a new Provider" do
          expect {
            post :create, {:provider => valid_attributes}, valid_session
          }.to change(Provider, :count).by(1)
        end

        it "assigns a newly created provider as @provider" do
          post :create, {:provider => valid_attributes}, valid_session
          assigns(:provider).should be_a(Provider)
          assigns(:provider).should be_persisted
        end

        it "redirects to the created provider" do
          post :create, {:provider => valid_attributes}, valid_session
          response.should redirect_to(Provider.last)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "assigns a newly created but unsaved provider as @provider" do
          # Trigger the behavior that occurs when invalid params are submitted
          Provider.any_instance.stub(:save).and_return(false)
          post :create, {:provider => { "nome" => "invalid value" }}, valid_session
          assigns(:provider).should be_a_new(Provider)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Provider.any_instance.stub(:save).and_return(false)
          post :create, {:provider => { "nome" => "invalid value" }}, valid_session
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "updates the requested provider" do
          provider = Provider.create! valid_attributes
          # Assuming there are no other providers in the database, this
          # specifies that the Provider created on the previous line
          # receives the :update_attributes message with whatever params are
          # submitted in the request.
          Provider.any_instance.should_receive(:update).with({ "nome" => "MyString" })
          put :update, {:id => provider.to_param, :provider => { "nome" => "MyString" }}, valid_session
        end

        it "assigns the requested provider as @provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => valid_attributes}, valid_session
          assigns(:provider).should eq(provider)
        end

        it "redirects to the provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => valid_attributes}, valid_session
          response.should redirect_to(provider)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
      
        it "assigns the provider as @provider" do
          provider = Provider.create! valid_attributes
          Provider.any_instance.stub(:save).and_return(false)
          put :update, {:id => provider.to_param, :provider => { "nome" => "invalid value" }}, valid_session
          assigns(:provider).should eq(provider)
        end

        it "re-renders the 'edit' template" do
          provider = Provider.create! valid_attributes
          Provider.any_instance.stub(:save).and_return(false)
          put :update, {:id => provider.to_param, :provider => { "nome" => "invalid value" }}, valid_session
          response.should render_template("edit")
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "destroys the requested provider" do
        provider = Provider.create! valid_attributes
        expect {
          delete :destroy, {:id => provider.to_param}, valid_session
        }.to change(Provider, :count).by(-1)
      end

      it "redirects to the providers list" do
        provider = Provider.create! valid_attributes
        delete :destroy, {:id => provider.to_param}, valid_session
        response.should redirect_to(providers_url)
      end
    end
  end
  
  context "Regular Logged user" do
    let(:valid_attributes) { FactoryGirl.attributes_for(:provider) }
    let(:valid_session) { {} }
    let(:user) { FactoryGirl.create(:user)}

    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns all providers as @providers" do
        provider = Provider.create! valid_attributes
        get :index, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET show" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns the requested provider as @provider" do
        provider = Provider.create! valid_attributes
        get :show, {:id => provider.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET new" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns a new provider as @provider" do
        get :new, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET edit" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "assigns the requested provider as @provider" do
        provider = Provider.create! valid_attributes
        get :edit, {:id => provider.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "creates a new Provider" do
          post :create, {:provider => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns a newly created provider as @provider" do
          post :create, {:provider => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the created provider" do
          post :create, {:provider => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "assigns a newly created but unsaved provider as @provider" do
          post :create, {:provider => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'new' template" do
          post :create, {:provider => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "updates the requested provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => { "nome" => "MyString" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "assigns the requested provider as @provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "redirects to the provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => valid_attributes}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end

      describe "with invalid params" do
        before do
          user.add_role(:default)
          user.save
          sign_in user
        end
      
        it "assigns the provider as @provider" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end

        it "re-renders the 'edit' template" do
          provider = Provider.create! valid_attributes
          put :update, {:id => provider.to_param, :provider => { "nome" => "invalid value" }}, valid_session
          expect(response).to redirect_to(permission_denied_path)
        end
      end
    end

    describe "DELETE destroy" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "destroys the requested provider" do
        provider = Provider.create! valid_attributes
        delete :destroy, {:id => provider.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end

      it "redirects to the providers list" do
        provider = Provider.create! valid_attributes
        delete :destroy, {:id => provider.to_param}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  end
  
  context "Guest user" do
    describe "GET index" do      
      it "should be redirected" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET show" do      
      it "should be redirected" do
        get :show, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET new" do
      it "should be redirected" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "GET edit" do
      it "should be redirected" do
        get :edit, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "should be redirected" do
          post :create
          expect(response).to redirect_to new_user_session_path
        end

      end

      describe "with invalid params" do
        it "should be redirected" do
          post :create, {:provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "MyString" }}
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "with invalid params" do
        it "should be redirected" do
          put :update, {:id => 1, :provider => { "nome" => "invalid value" }}
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    describe "DELETE destroy" do
      it "should be redirected" do
        delete :destroy, {:id => 1}
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

end
