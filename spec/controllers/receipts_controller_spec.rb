require 'spec_helper'

describe ReceiptsController do
  let(:user) { FactoryGirl.create(:user) }

  context 'Admin Logged user' do
    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user
      end

      it "returns http success" do
        get :index
        expect(response).to be_success
      end
    end

    describe "GET 'new'" do
      let(:pre_receipt) { mock_model PreReceipt }
      
      before do
        PreReceipt.should_receive(:find).and_return(pre_receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        get :new, :id => 1
        expect(response).to be_success
      end
    end
    
    describe "POST 'create'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        receipt.should_receive(:pre_receipt=)
        receipt.should_receive(:save).and_return(true)
        Receipt.should_receive(:new).twice.and_return(receipt)
        PreReceipt.should_receive(:find).and_return(pre_receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        post :create, :receipt => attributes  
        expect(response).to be_redirect
      end
    end
    
    describe "POST 'create'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        receipt.should_receive(:pre_receipt=)
        receipt.should_receive(:save).and_return(false)
        Receipt.should_receive(:new).twice.and_return(receipt)
        PreReceipt.should_receive(:find).and_return(pre_receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        post :create, :receipt => attributes  
        expect(response).to be_success
      end
    end
    
    describe "PATCH 'update'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        receipt.should_receive(:update).and_return(true)
        Receipt.should_receive(:find).twice.and_return(receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        patch :update, :id => 1, :receipt => attributes
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH 'update'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        receipt.should_receive(:update).and_return(false)
        Receipt.should_receive(:find).twice.and_return(receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        patch :update, :id => 1, :receipt => attributes
        expect(response).to be_success
      end
    end

    describe "GET 'edit'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt }
      
      before do
        receipt.should_receive(:pre_receipt).and_return(pre_receipt)
        Receipt.should_receive(:find).twice.and_return(receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        get :edit, :id => 1
        expect(response).to be_success
      end
    end
    describe "DELETE destroy" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}

      before do        
        Receipt.should_receive(:find).twice.and_return(receipt)
        user.add_role(:admin)
        user.save
        sign_in user
      end

      describe "when no error occurs" do
        it "redirects to receipts page" do
          receipt.should_receive(:destroy).and_return(true)
          delete :destroy, :id => 1
          expect(response).to be_redirect      
        end
      end      
      describe "when error occurs" do
        it "redirects to receipts page" do
          receipt.should_receive(:destroy).and_return(false)
          delete :destroy, :id => 1
          expect(response).to be_redirect      
        end
      end      
    end  
  end
  
  context 'Regular Logged user' do
    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end

      it "returns http success" do
        get :index
        expect(response).to be_redirect
      end
    end

    describe "GET 'new'" do
      let(:pre_receipt) { mock_model PreReceipt }
      
      before do
        #PreReceipt.should_receive(:find).and_return(pre_receipt)
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        get :new, :id => 1
        expect(response).to be_redirect
      end
    end
    
    describe "POST 'create'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        post :create, :receipt => attributes  
        expect(response).to be_redirect
      end
    end
    
    describe "POST 'create'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        attributes = { :avarias => true }
        post :create, :receipt => attributes  
        expect(response).to be_redirect
      end
    end

    describe "GET 'edit'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt }
      
      before do        
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do  
        Receipt.should_receive(:find).and_return(Receipt.new)
        get :edit, :id => 1
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH 'update'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        Receipt.should_receive(:find).and_return(Receipt.new)
        attributes = { :avarias => true }
        patch :update, :id => 1, :receipt => attributes
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH 'update'" do
      let(:pre_receipt) { mock_model PreReceipt }
      let(:receipt) { mock_model Receipt}
      
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end
      
      it "returns http success" do
        Receipt.should_receive(:find).and_return(Receipt.new)
        attributes = { :avarias => true }
        patch :update, :id => 1, :receipt => attributes
        expect(response).to be_redirect
      end
    end
  end
  
  context 'Guest user' do
    describe "GET index" do
      it "returns http success" do
        get :index
        expect(response).to be_redirect
      end
    end

    describe "GET 'new'" do
      it "returns http success" do
        get :new, :id => 1
        expect(response).to be_redirect
      end
    end

    describe "GET 'edit'" do
      it "returns http success" do
        get :edit, :id => 1
        expect(response).to be_redirect
      end
    end
    
    describe "POST 'create'" do
      it "returns http success" do
        attributes = { :avarias => true }
        post :create, :receipt => attributes
        expect(response).to be_redirect
      end
    end
    
    describe "PATCH 'update'" do
      it "returns http success" do
        attributes = { :avarias => true }
        patch :update, :id => 1, :receipt => attributes
        expect(response).to be_redirect
      end
    end
  end

end