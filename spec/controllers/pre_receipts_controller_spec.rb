# -*- encoding : utf-8 -*-
require "spec_helper"

describe PreReceiptsController do

	context "Admin is logged"
		let(:valid_attributes) { FactoryGirl.attributes_for(:pre_receipt)}
		let(:unprocessable_entity) {422}			
    
		let(:product_valid_attributes) {{:id => 1, :name => "MyString", 
				:codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1) }}

    	let(:valid_session) { }
    	let(:user) { FactoryGirl.create(:user)}

    	describe "GET index" do
    		before do
			    user.add_role(:admin)
			    user.save
				  sign_in user
			  end
        
    		it "assigns all pre_receipts as @pre_receipts" do
    			pre_receipt = PreReceipt.create! valid_attributes
    			get :index, {}, valid_session
    			assigns(:pre_receipts).should eq([pre_receipt])
    		end
    	end

    	describe "GET show" do
    		before do
			    user.add_role(:admin)
			    user.save
  				sign_in user
  			end
        
  			it "assigns the requested pre_receipt as @pre_receipt" do
  				pre_receipt = PreReceipt.create! valid_attributes
  				get :show, {:id => pre_receipt.to_param}, valid_session
  				assigns(:pre_receipt).should eq(pre_receipt)
  			end
    	end

    	describe "GET new" do
    		before do
			    user.add_role(:admin)
			    user.save
			    sign_in user
          address = mock_model(Address)
          address.should_receive(:address).and_return("Rua 1")
          Address.should_receive(:all).and_return([ address ])
        end
  			it "assigns a new pre_receipt as @pre_receipt" do
  				get :new, {}, valid_session
  				assigns(:pre_receipt).should be_a_new(PreReceipt)
  			end
    	end

    	describe "GET edit" do
    		before do
			    user.add_role(:admin)
			    user.save
				  sign_in user
        end
        
  			it "assigns the requested pre_receipt as @pre_receipt" do
  				pre_receipt = PreReceipt.create! valid_attributes
  				get :edit, {:id => pre_receipt.to_param}, valid_session
  				assigns(:pre_receipt).should eq(pre_receipt)
  			end			
		end

		describe "POST create" do
			describe "with valid params" do
				before do
				  user.add_role(:admin)
				  user.save
					sign_in user
					product = Product.create! product_valid_attributes
					Random.should_receive(:rand).with(any_args()).and_return(763)
				end
        
				it "creates a new pre_receipt" do
					expect {
						post :create, {:pre_receipt => valid_attributes}, valid_session
					}.to change(PreReceipt, :count).by(1)
				end			
        	
				it "responses a pre_receipt as json" do
					pre_receipt = PreReceipt.new(valid_attributes)
					post :create, {:pre_receipt => valid_attributes, :format => :json}, valid_session
					expected = pre_receipt.to_json(:include => {:product => { :only => :name }, :address => { :only => :nome }})					
          expect(response.body).to include(valid_attributes[:product_id].to_s)
				end
			end						
			describe "with invalid params" do
				before do
          user.add_role(:admin)
          user.save
          sign_in user
          product = Product.create! product_valid_attributes					
				end
        
				it "responses error status" do
					PreReceipt.any_instance.stub(:save).and_return(false)
					post :create, {:pre_receipt => {:packing => "10", :quantidade => "2", :product_id => "1"}, :format => :json}, valid_session			
					expect(response.status).to eq(unprocessable_entity)
				end
			end			
		end

		describe "PUT update" do
			describe "with valid params" do
				before do
          user.add_role(:admin)
          user.save
          sign_in user
        end
				it "updates the requested pre_receipt" do
		          pre_receipt = PreReceipt.create! valid_attributes
		          PreReceipt.any_instance.should_receive(:update).with({ "quantidade" => "10" })
		          put :update, {:id => pre_receipt.to_param, :pre_receipt => { "quantidade" => "10" }}, valid_session
		        end

		        it "assigns the requested pre_receipt as @pre_receipt" do
		          pre_receipt = PreReceipt.create! valid_attributes
		          put :update, {:id => pre_receipt.to_param, :pre_receipt => valid_attributes}, valid_session
		          assigns(:pre_receipt).should eq(pre_receipt)
		        end

		        it "redirects to the pre_receipt" do
		          pre_receipt = PreReceipt.create! valid_attributes
		          put :update, {:id => pre_receipt.to_param, :pre_receipt => valid_attributes}, valid_session
		          response.should redirect_to(pre_receipt)
		        end
			end
			describe "with invalid params" do
        before do
          user.add_role(:admin)
          user.save
          sign_in user
        end

        it "assigns the pre_receipt as @pre_receipt" do
          pre_receipt = PreReceipt.create! valid_attributes
          PreReceipt.any_instance.stub(:save).and_return(false)
          put :update, {:id => pre_receipt.to_param, :pre_receipt => { "quantidade" => "invalid value" }}, valid_session
          assigns(:pre_receipt).should eq(pre_receipt)
        end

        it "re-renders the 'edit' template" do
          pre_receipt = PreReceipt.create! valid_attributes
          PreReceipt.any_instance.stub(:save).and_return(false)
          put :update, {:id => pre_receipt.to_param, :pre_receipt => { "quantidade" => "invalid value" }}, valid_session
          response.should render_template("edit")
        end
      end
		end
    
		describe "DELETE destroy" do    
			before do
				user.add_role(:admin)
				user.save
				sign_in user
			end
      describe "when uses html request" do
  			it "destroys the requested pre_receipt" do
  				pre_receipt = PreReceipt.create! valid_attributes
  				expect {
  					delete :destroy, {:id => pre_receipt.to_param}, valid_session
  				}.to change(PreReceipt, :count).by(-1)
  			end
        
  			it "redirects to the pre_receipts list" do
  				pre_receipt = PreReceipt.create! valid_attributes
  				delete :destroy, {:id => pre_receipt.to_param}, valid_session
  				response.should redirect_to(pre_receipts_url)
  			end
      end

      describe "when uses json request" do
        it "responses a json with ok status" do
          pre_receipt = PreReceipt.create! valid_attributes
          delete :destroy, {:id => pre_receipt.to_param, :format => :json}, valid_session
          expected = {status: 'ok'}
          expect(response.body).to eq(expected.to_json)
        end
      end
		end	
	context 'Regular Logged user' do
		before do
      user.add_role(:default)
      user.save
      sign_in user
      pre_receipt = PreReceipt.create! valid_attributes
    end
		describe "GET index" do	    
      it "should be redirected" do	        
        get :index, {}, valid_session
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "GET show" do  
      let(:pre_receipt) { mock_model PreReceipt }
      
      before do
        PreReceipt.should_receive(:find).and_return(pre_receipt)
      end
       
      it "should be redirected" do
        get :show, {:id => 1}, valid_session
        expect(response).to redirect_to permission_denied_path
      end
    end

    describe "GET new" do 	
      it "should be redirected" do
        get :new, valid_session
        expect(response).to redirect_to permission_denied_path
      end
    end

    describe "GET edit" do
      let(:pre_receipt) { mock_model PreReceipt }
      
      before do
        PreReceipt.should_receive(:find).and_return(pre_receipt)
      end
      
      it "should be redirected" do
        get :edit, {:id => 1}, valid_session
        expect(response).to redirect_to permission_denied_path
      end
    end
	    
    describe "POST create" do    	
      describe "with valid params" do
        it "should be redirected" do
          post :create, valid_session
          expect(response).to redirect_to permission_denied_path
        end

      end

      describe "with invalid params" do	      	
        it "should be redirected" do
          post :create, {:pre_receipt => { "quantidade" => "invalid value" }}, valid_session
          expect(response).to redirect_to permission_denied_path
        end
      end
    end

    describe "PUT update" do	    	
      describe "with valid params" do
        let(:pre_receipt) { mock_model PreReceipt }
      
        before do
          PreReceipt.should_receive(:find).and_return(pre_receipt)
        end
        
        it "should be redirected" do
          put :update, {:id => 1, :pre_receipt => { "quantidade" => "10" }}, valid_session
          expect(response).to redirect_to permission_denied_path
        end
      end

      describe "with invalid params" do	   
        let(:pre_receipt) { mock_model PreReceipt }
      
        before do
          PreReceipt.should_receive(:find).and_return(pre_receipt)
        end
         	
        it "should be redirected" do
          put :update, {:id => 1, :pre_receipt => { "quantidade" => "invalid value" }}, valid_session
          expect(response).to redirect_to permission_denied_path
        end
      end
    end

    describe "DELETE destroy" do   
      let(:pre_receipt) { mock_model PreReceipt }
      
      before do
        PreReceipt.should_receive(:find).and_return(pre_receipt)
      end
         	  	    		    
      it "should be redirected" do
        delete :destroy, {:id => 1}, valid_session
        expect(response).to redirect_to permission_denied_path
      end
    end
	end
end