require 'spec_helper'

describe ProductReportsController do
  let(:user) { FactoryGirl.create(:user) }

  context 'Admin Logged user'
    describe "GET index" do
      before do
        user.add_role(:admin)
        user.save
        sign_in user

        @product_type = FactoryGirl.build(:product_type)
        @provider = FactoryGirl.build(:provider)
        ProductType.should_receive(:all).and_return([@product_type])
        Provider.should_receive(:all).and_return([@provider])
      end      
      it "returns all product_types as @product_types" do
        get :index, {}
        assigns(:product_types).should eq([@product_type])
      end
      it "returns all providers as @providers" do
        get :index, {}
        assigns(:providers).should eq([@provider])
      end
    end
    describe "GET report" do
      before do
        user.add_role(:admin)
        user.save
        RestClient.should_receive(:post).with(any_args()).and_return("somevalue")
        sign_in user
      end
      
      it "should return success" do
        get :report, :format => :pdf
        expect(response).to be_success
      end
    end
  context 'Default Logged user'    
    describe "GET index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        get :index, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        post :report, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end
  context 'Guest Logged user'    
    describe "GET index" do
      before do
        user.add_role(:guest)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        get :index, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end

    describe "POST index" do
      before do
        user.add_role(:default)
        user.save
        sign_in user
      end 
      it "redirects to permission denied page" do
        post :report, {}
        expect(response).to redirect_to(permission_denied_path)
      end
    end    
end
