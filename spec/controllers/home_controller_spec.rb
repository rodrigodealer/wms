# -*- encoding : utf-8 -*-
require 'spec_helper'

describe HomeController do
  
  context "Logged user" do
    describe "GET 'index'" do
      let(:user) { FactoryGirl.create(:user) }
      
      before do
        sign_in user
      end
      
      it "should return success" do
        get :index
        expect(response).to be_success
      end
    end
  end
  
  context "Guest user" do
    describe "GET 'index'" do
      it "should return success" do
        get :index
        expect(response).to_not be_success
      end
    end
  end

end
