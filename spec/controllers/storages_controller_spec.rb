require 'spec_helper'

describe StoragesController do

	let(:valid_session) { }
    let(:user) { FactoryGirl.create(:user)}

    context "Admin is logged"
		let(:data) {'30/08/2013'}
		let(:valid_address) {{id: 1, nome: 'Endereco de teste'}}
		let(:construcao) {{:id => 1, :descricao => "Construcao"}}
		let(:cimento) {{:id => 1, :name => "Cimento", :codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1) }}
		let(:tijolo) {{:id => 2, :name => "Tijolo", :codigo => '3557', :product_type_id => "1", :provider => Provider.new(:id => 1) }}

		describe "GET index" do
			before do
				user.add_role(:admin)
        		user.save
        		sign_in user

				@product_cimento = Product.create! cimento
				@product_tijolo = Product.create! tijolo
				@construcao = ProductType.create! construcao			
				Address.create! valid_address				
				@addresses = Address.all.to_set.classify {
					|address| address.address
				}
			end

			it "returns all products as @products" do
				get :index, {}
				assigns(:products).should eq([@product_cimento, @product_tijolo])
			end
			it "returns all product_types as @product_types" do
				get :index, {}
				assigns(:product_types).should eq([@construcao])
			end
			it "returns all addresses as @addresses" do
				get :index, {}
				assigns(:addresses).should eq(@addresses)
			end
		end

		describe "GET locate" do

			before do
				user.add_role(:admin)
        		user.save
        		sign_in user

				@product_cimento = Product.new cimento
				@product_tijolo = Product.new tijolo				
			end

			it "returns the storages as json" do
				storage_cimento = FactoryGirl.build(:storage, product: @product_cimento, receipt: 10, dispatch: 15, storage: -5)
				storage_tijolo = FactoryGirl.build(:storage, product: @product_tijolo, receipt: 20, dispatch: 35, storage: -15)
				Storage.any_instance.stub(:locate).with(any_args()).and_return([storage_cimento, storage_tijolo])
				get :locate, {:data => data, :format => :json}
				expected = [storage_cimento, storage_tijolo].to_json
				expect(response.body).to eq(expected)			
			end
		end
		describe "GET report" do
	      before do
	        user.add_role(:admin)
	        user.save
	        RestClient.should_receive(:post).with(any_args()).and_return("somevalue")
	        Storage.any_instance.should_receive(:locate).with(any_args()).and_return([])
	        sign_in user
	      end
	      
	      it "should return success" do
	        get :report, :format => :pdf
	        expect(response).to be_success
	      end
	    end
	context "Default user is logged"
		describe "GET locate" do
			before do
				user.add_role(:default)
        		user.save
        		sign_in user
			end
			it "redirects to permission denied page" do
				get :locate, :data => data
				expect(response).to redirect_to permission_denied_path
			end
		end
		describe "GET report" do
	      before do
	        user.add_role(:default)
	        user.save	        
	        sign_in user
	      end
	      
	      it "should return success" do
	        get :report, :format => :pdf
	        expect(response).to redirect_to permission_denied_path
	      end
	    end
	context "Guest user is logged"
		describe "GET locate" do
			before do
				user.add_role(:guest)
        		user.save
        		sign_in user        		
			end
			it "redirects to permission denied page" do
				get :locate, :data => data
				expect(response).to redirect_to permission_denied_path
			end
		end
		describe "GET report" do
	      before do
	        user.add_role(:guest)
	        user.save	        
	        sign_in user
	      end
	      
	      it "should return success" do
	        get :report, :format => :pdf
	        expect(response).to redirect_to permission_denied_path
	      end
	    end
end
