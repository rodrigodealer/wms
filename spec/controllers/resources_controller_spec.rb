# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ResourcesController do


  context 'Logged user' do
    let(:user) { FactoryGirl.create(:user)}
    describe "GET 'language'" do
      before do
        sign_in user
      end
    
      it "returns http success" do
        get :language, :language => "pt_BR"
        response.should be_success
      end
    end
  end
  
  context 'Guest user' do
    describe "GET 'language'" do
      it "returns http success" do
        get :language, :language => "pt_BR"
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
