require 'spec_helper'

describe Dispatch do
  
  describe "Validations" do
  	it {should validate_presence_of :expedicao}
  	it {should validate_presence_of :quantidade}
  	it {should validate_presence_of :solicitante}
  	it {should validate_presence_of :conferente}
  	it {should validate_presence_of :conferente_adicional}
  	it {should validate_presence_of :status}
  end
  describe "Associations" do
  	it {should belong_to :product}
    it {should belong_to :address}
  end

  let(:cimento) {{:id => 1, :name => "Cimento", :codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1) }}
  let(:valid_cimento_address) {{id: 1, nome: "Endereco dos cimentos"}}

  let(:valid_pre_receipt_cimento) {{:id => 1, :product_id => 1, :quantidade => 20, :embarque => "08/08/2013", 
    :rastreio => "1", :fabricacao => "06/08/2013", :packing => "10",
    :container => "AC", :status => "Embarcado", :address_id => 1}}

  let(:valid_receipt_cimento) {{:pre_receipt => PreReceipt.new(valid_pre_receipt_cimento), :rastreio => "10", 
                                :avarias => "1", :qtde_avariada => "2", :data_recebimento => "08/08/2013"}}
  let(:valid_receipt_tijolo) {{:pre_receipt => PreReceipt.new(valid_pre_receipt_tijolo), :rastreio => "101", 
                                :avarias => "1", :qtde_avariada => "5", :data_recebimento => "08/08/2013"}}

  let(:valid_dispatch_cimento) {{:id => 1, :product_id => 1, :quantidade => 15, :expedicao => "08/08/2013",      
    :solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => "1"}}
  let(:full_dispatch_cimento) {{:id => 1, :product_id => 1, :quantidade => 20, :expedicao => "08/08/2013",      
    :solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => "1"}}
  let(:new_dispatch) {{:product_id => 1, :quantidade => 2, :expedicao => "08/08/2013",      
    :solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => "1"}}

  before do
    address = Address.create(valid_cimento_address)
    product_cimento = Product.create! cimento        
  end

  context "disptach product has storage"

    describe "#saida!" do

      before do

        Dispatch.delete_all
        Receipt.delete_all

        receipt_cimento = Receipt.create(valid_receipt_cimento)
        dispatch_cimento = Dispatch.create(valid_dispatch_cimento)
      end      

      it "creates a new dispatch" do
        dispatch = Dispatch.new new_dispatch
        expect(dispatch.saida!).to be(true)
      end

    end
  context "disptach product has not enough storage"

      before do

        Dispatch.delete_all
        Receipt.delete_all                
      end

      describe "#saida!" do
        it "doesn't create a new dispatch" do        
          Receipt.create!(valid_receipt_cimento)
          Dispatch.create!(full_dispatch_cimento)

          dispatch = Dispatch.new new_dispatch
          expect{dispatch.saida!}.to raise_error(RuntimeError, "produto nao disponivel em estoque neste endereco.")
        end
      end
  context "dispatch product has no one storage"
    before do

        Dispatch.delete_all
        Receipt.delete_all
        
      end
    it "doesn't create a new dispatch" do
      dispatch = Dispatch.new new_dispatch
      expect{dispatch.saida!}.to raise_error(RuntimeError, "produto nao disponivel em estoque neste endereco.")
    end
end
