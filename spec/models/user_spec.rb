# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User do
  describe "Validations" do
    it { should_not allow_value("blah").for(:email) }
    it { should allow_value("a@b.com").for(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should_not allow_value("123456").for(:password) }
    it { should_not allow_value("123456").for(:password_confirmation) }
    it { should allow_value("12345678").for(:password) }
  end
end
