require 'spec_helper.rb'

describe Storage do
	let(:data) {'30/08/2013'}
	let(:valid_cimento_address) {{id: 1, nome: "Endereco dos cimentos"}}
	let(:valid_tijolo_address) {{id: 2, nome: "Endereco dos tijolos"}}
	let(:cimento) {{:id => 1, :name => "Cimento", :codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1)}}
	let(:tijolo) {{:id => 2, :name => "Tijolo", :codigo => '3557', :product_type_id => "2", :provider => Provider.new(:id => 1)}}
		

	let(:valid_pre_receipt_cimento) {{:id => 1, :product_id => 1, :quantidade => 10, :embarque => "08/08/2013", 
			:rastreio => "1", :fabricacao => "06/08/2013", :packing => "10", 
			:container => "AC", :status => "Embarcado", :address_id => 1}}
	let(:other_pre_receipt_cimento) {{:id => 3, :product_id => 1, :quantidade => 30, :embarque => "08/08/2013", 
			:rastreio => "1", :fabricacao => "06/08/2013", :packing => "10",
			:container => "AC", :status => "Embarcado", :address_id => 1}}				
	let(:valid_pre_receipt_tijolo) {{:id => 2, :product_id => 2, :quantidade => 20, :embarque => "08/08/2013", 
			:rastreio => "1", :fabricacao => "06/08/2013", :packing => "10",
			:container => "AC", :status => "Embarcado", :address_id => 2}}

	let(:valid_receipt_cimento) {{:pre_receipt => PreReceipt.new(valid_pre_receipt_cimento), :rastreio => "10", :avarias => "1", :qtde_avariada => "2", :data_recebimento => "08/08/2013"}}
	let(:other_valid_receipt_cimento) {{:pre_receipt => PreReceipt.new(other_pre_receipt_cimento), :rastreio => "10", :avarias => "0", :qtde_avariada => "0", :data_recebimento => "08/08/2013"}}
	let(:valid_receipt_tijolo) {{:pre_receipt => PreReceipt.new(valid_pre_receipt_tijolo), :rastreio => "101", :avarias => "1", :qtde_avariada => "5", :data_recebimento => "08/08/2013"}}

	let(:valid_dispatch_cimento) {{:id => 1, :product_id => 1, :quantidade => 15, :expedicao => "08/08/2013",
	 	:solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => 1}}
	let(:valid_dispatch_tijolo) {{:id => 2, :product_id => 2, :quantidade => 35, :expedicao => "08/08/2013", 
		:solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => 2}}		
	let(:other_valid_dispatch_tijolo) {{:id => 3, :product_id => 2, :quantidade => 22, :expedicao => "08/08/2013", 
		:solicitante => "Pedro", :conferente => "Pedro", :conferente_adicional => "Rodrigo", :status => "Saida", :address_id => 2}}

	before do
		@product_cimento = Product.create(cimento)
		@product_tijolo = Product.create(tijolo)
		Address.create(valid_cimento_address)
		Address.create(valid_tijolo_address)
	end	

	context "exist only receipts"		
		describe "#locate" do
			before do				
				receipt_cimento = Receipt.create(valid_receipt_cimento)
				receipt_tijolo = Receipt.create(valid_receipt_tijolo)
				Dispatch.delete_all

				cimento_history_receipts = Array.new				
				cimento_history_receipts << FactoryGirl.build(:history_receipt, receipt: receipt_cimento, pre_receipt: receipt_cimento.pre_receipt)

				tijolo_history_receipts = Array.new
				tijolo_history_receipts << FactoryGirl.build(:history_receipt, receipt: receipt_tijolo, pre_receipt: receipt_tijolo.pre_receipt)

				@storage_cimento = FactoryGirl.build(:storage, product: @product_cimento, receipt: 8, dispatch: 0, storage: 8, history_receipts: cimento_history_receipts)
				@storage_tijolo = FactoryGirl.build(:storage, product: @product_tijolo, receipt: 15, dispatch: 0, storage: 15, history_receipts: tijolo_history_receipts)
			end
      
			it "returns products storages with receipts" do
				storage = Storage.new
				storages = storage.locate({:data => data})

				expect(storages[0].product).to eq(@storage_cimento.product)
				expect(storages[0].receipt).to eq(@storage_cimento.receipt)
				expect(storages[0].dispatch).to eq(@storage_cimento.dispatch)
				expect(storages[0].storage).to eq(@storage_cimento.storage)

				expect(storages[1].product).to eq(@storage_tijolo.product)
				expect(storages[1].receipt).to eq(@storage_tijolo.receipt)
				expect(storages[1].dispatch).to eq(@storage_tijolo.dispatch)
				expect(storages[1].storage).to eq(@storage_tijolo.storage)
			end
      
			it "groups products and sum receipts" do
				Receipt.create! other_valid_receipt_cimento
				storage = Storage.new
				storages = storage.locate({:data => data})
				expect(storages[0].product).to eq(@storage_cimento.product)
				expect(storages[0].receipt).to eq(38)
				expect(storages[0].dispatch).to eq(@storage_cimento.dispatch)
				expect(storages[0].storage).to eq(38)

				expect(storages[1].product).to eq(@storage_tijolo.product)
				expect(storages[1].receipt).to eq(@storage_tijolo.receipt)
				expect(storages[1].dispatch).to eq(@storage_tijolo.dispatch)
				expect(storages[1].storage).to eq(@storage_tijolo.storage)
			end

			it "returns products receipts history" do
				other_receipt = Receipt.create! other_valid_receipt_cimento
				@storage_cimento.history_receipts << FactoryGirl.build(:history_receipt, receipt: other_receipt, pre_receipt: other_receipt.pre_receipt)
				storage = Storage.new
				storages = storage.locate({:data => data})

				expect(storages[0].history_receipts[0].pre_receipt.quantidade).to eq(@storage_cimento.history_receipts[0].pre_receipt.quantidade)
				expect(storages[0].history_receipts[1].pre_receipt.quantidade).to eq(@storage_cimento.history_receipts[1].pre_receipt.quantidade)

				expect(storages[1].history_receipts[0].pre_receipt.quantidade).to eq(@storage_tijolo.history_receipts[0].pre_receipt.quantidade)
			end
		end
	context "exist receipts and dispatches"
		describe "#locate" do
			before do
				PreReceipt.delete_all
				Dispatch.delete_all
				receipt_cimento = Receipt.create(valid_receipt_cimento)
				receipt_tijolo = Receipt.create(valid_receipt_tijolo)
				dispatch_cimento = Dispatch.create(valid_dispatch_cimento)
				dispatch_tijolo = Dispatch.create(valid_dispatch_tijolo)

				cimento_history_receipts = Array.new				
				cimento_history_receipts << FactoryGirl.build(:history_receipt, receipt: receipt_cimento, pre_receipt: receipt_cimento.pre_receipt)
				tijolo_history_receipts = Array.new
				tijolo_history_receipts << FactoryGirl.build(:history_receipt, receipt: receipt_tijolo, pre_receipt: receipt_tijolo.pre_receipt)

				cimento_history_dispatches = Array.new
				cimento_history_dispatches << dispatch_cimento
				tijolo_history_dispatches = Array.new
				tijolo_history_dispatches << dispatch_tijolo

				@storage_cimento = FactoryGirl.build(:storage, product: @product_cimento, receipt: 8, dispatch: 15, storage: -7, 
													 history_receipts: cimento_history_receipts, history_dispatches: cimento_history_dispatches)

				@storage_tijolo = FactoryGirl.build(:storage, product: @product_tijolo, receipt: 15, dispatch: 35, storage: -20, 
													history_receipts: tijolo_history_receipts, history_dispatches: tijolo_history_dispatches)
			end
      
			it "returns products storages" do
				storage = Storage.new
				storages = storage.locate({:data => data})
				expect(storages[0].product).to eq(@storage_cimento.product)
				expect(storages[0].receipt).to eq(@storage_cimento.receipt)
				expect(storages[0].dispatch).to eq(@storage_cimento.dispatch)
				expect(storages[0].storage).to eq(@storage_cimento.storage)

				expect(storages[1].product).to eq(@storage_tijolo.product)
				expect(storages[1].receipt).to eq(@storage_tijolo.receipt)
				expect(storages[1].dispatch).to eq(@storage_tijolo.dispatch)
				expect(storages[1].storage).to eq(@storage_tijolo.storage)
			end

			it "returns products storages histories" do
				storage = Storage.new
				storages = storage.locate({:data => data})

				expect(storages[0].history_receipts[0].pre_receipt.quantidade).to eq(@storage_cimento.history_receipts[0].pre_receipt.quantidade)
				expect(storages[0].history_dispatches[0].quantidade).to eq(@storage_cimento.history_dispatches[0].quantidade)

				expect(storages[1].history_receipts[0].pre_receipt.quantidade).to eq(@storage_tijolo.history_receipts[0].pre_receipt.quantidade)
				expect(storages[1].history_dispatches[0].quantidade).to eq(@storage_tijolo.history_dispatches[0].quantidade)
			end
		end		
	context "don't exist receipts and dispatches"
		describe "#locate" do
			before do
				Dispatch.delete_all
				Receipt.delete_all
			end
      
			it "returns none storages" do
				storage = Storage.new
				storages = storage.locate({:data => data})
				expect(storages.size).to eq(0)
			end
		end
	context "storage is filtered by Product Type"
		describe "#locate" do

			before do
				Dispatch.delete_all
				Receipt.delete_all

				receipt_cimento = Receipt.create(valid_receipt_cimento)
				receipt_tijolo = Receipt.create(valid_receipt_tijolo)
				dispatch_cimento = Dispatch.create(valid_dispatch_cimento)
				dispatch_tijolo = Dispatch.create(valid_dispatch_tijolo)

				@storage_tijolo = FactoryGirl.build(:storage, product: @product_tijolo, receipt: 15, dispatch: 35, storage: -20)
			end

			it "returns all products storages from the selected product type" do
				storage = Storage.new				
				storages = storage.locate({:data => data, :product_type_id => 2})				

				expect(storages[0].product).to eq(@storage_tijolo.product)
				expect(storages[0].receipt).to eq(@storage_tijolo.receipt)
				expect(storages[0].dispatch).to eq(@storage_tijolo.dispatch)
				expect(storages[0].storage).to eq(@storage_tijolo.storage)
			end

			it "returns empty when doesn't find product storages for selected product type" do
				storage = Storage.new				
				storages = storage.locate({:data => data, :product_type_id => 3})								
				storages.size.should eq(0)
			end
	 	end
	context "storage is filtered by product"
		describe "#locate" do
			
			before do
				Dispatch.delete_all
				Receipt.delete_all

				receipt_cimento = Receipt.create(valid_receipt_cimento)
				receipt_tijolo = Receipt.create(valid_receipt_tijolo)
				dispatch_cimento = Dispatch.create(valid_dispatch_cimento)
				dispatch_tijolo = Dispatch.create(valid_dispatch_tijolo)

				@storage_cimento = FactoryGirl.build(:storage, product: @product_cimento, receipt: 8, dispatch: 15, storage: -7)
			end

			it "returns product storage" do
				storage = Storage.new				
				storages = storage.locate({:data => data, :product_id => 1})

				expect(storages.size).to eq(1)
				expect(storages[0].product).to eq(@storage_cimento.product)
				expect(storages[0].receipt).to eq(@storage_cimento.receipt)
				expect(storages[0].dispatch).to eq(@storage_cimento.dispatch)
				expect(storages[0].storage).to eq(@storage_cimento.storage)
			end

			it "returns empty when doesn't find product storages for selected product" do
				storage = Storage.new				
				storages = storage.locate({:data => data, :product_id => 3})

				expect(storages.size).to eq(0)
			end

		end
	context "storage is filtered by address"
		describe "when storage is not empty" do
			before do
				Dispatch.delete_all
				Receipt.delete_all

				receipt_cimento = Receipt.create(valid_receipt_cimento)
				receipt_tijolo = Receipt.create(valid_receipt_tijolo)
				dispatch_cimento = Dispatch.create(valid_dispatch_cimento)
				dispatch_tijolo = Dispatch.create(valid_dispatch_tijolo)

				@storage_cimento = FactoryGirl.build(:storage, product: @product_cimento, receipt: 8, dispatch: 15, storage: -7)
			end
			it "returns product storage" do
				storage = Storage.new				
				storages = storage.locate({:data => data, :address_id => 1})

				expect(storages.size).to eq(1)
				expect(storages[0].product).to eq(@storage_cimento.product)
				expect(storages[0].receipt).to eq(@storage_cimento.receipt)
				expect(storages[0].dispatch).to eq(@storage_cimento.dispatch)
				expect(storages[0].storage).to eq(@storage_cimento.storage)
			end			
		end
end