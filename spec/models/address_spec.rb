# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Address do
  describe "Validations" do
    it { should validate_presence_of :nome }

    it { should have_attached_file(:file) }
  end
  
  describe "Associations" do
    it { should have_many(:addresses).dependent(:destroy) }
    it { should belong_to(:address) }
    
    it "should create address with association" do
      address = FactoryGirl.create(:address)
      address2 = FactoryGirl.build(:address, :address => address)
      expect(address2.save).to be_true
    end
  end
  
  context "With parents" do
    describe "parent_count" do
      before do
        address1 = FactoryGirl.create(:address)
        address2 = FactoryGirl.create(:address)
        address4 = FactoryGirl.create(:address, :address => address2)
        @address = FactoryGirl.create(:address, :address => address4)
      end
    
      it "should create parent count" do
        result = @address.parent_addresses()
        expect(result).to be_eql(2)
      end
    end
  end
  
  context "Without parents" do
    describe "parent_count" do
      let(:address) { FactoryGirl.create(:address) }
    
      it "should create parent count" do
        result = address.parent_addresses()
        expect(result).to be_eql(0)
      end
    end
  end
  
  
end
