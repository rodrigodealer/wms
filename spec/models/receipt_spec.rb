# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Receipt do
	describe "Validations" do
		it { should validate_presence_of :rastreio }	
		it { should validate_presence_of :data_recebimento }	
    it { should_not validate_presence_of :avarias }
    it { should_not validate_presence_of :qtde_avariada }
    it { should_not validate_presence_of :descricao_avarias }
    it { should_not validate_presence_of :placa_caminhao }
    it { should_not validate_presence_of :pre_receipt_id }
	end

	describe "Associations" do
		it { should belong_to :pre_receipt }
	end

	context "receipts report" 
		let(:valid_cimento_address) {{id: 1, nome: "Endereco dos cimentos"}}
		let(:valid_tijolo_address) {{id: 2, nome: "Endereco dos tijolos"}}
		let(:cimento) {{:id => 1, :name => "Cimento", :codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1)}}
		let(:tijolo) {{:id => 2, :name => "Tijolo", :codigo => '3557', :product_type_id => "2", :provider => Provider.new(:id => 1)}}
			

		let(:valid_pre_receipt_cimento) {{:id => 1, :product_id => 1, :quantidade => 10, :embarque => "08/08/2013", 
				:rastreio => "1", :fabricacao => "06/08/2013", :packing => "10", 
				:container => "AC", :status => "Embarcado", :address_id => 1}}
		let(:other_pre_receipt_cimento) {{:id => 3, :product_id => 1, :quantidade => 30, :embarque => "08/08/2013", 
				:rastreio => "1", :fabricacao => "06/08/2013", :packing => "10",
				:container => "AC", :status => "Embarcado", :address_id => 1}}				
		let(:valid_pre_receipt_tijolo) {{:id => 2, :product_id => 2, :quantidade => 20, :embarque => "08/08/2013", 
				:rastreio => "1", :fabricacao => "06/08/2013", :packing => "40",
				:container => "AC", :status => "Embarcado", :address_id => 2}}

		let(:valid_receipt_cimento) {{:pre_receipt => PreReceipt.create!(valid_pre_receipt_cimento), :rastreio => "10", :avarias => "1", :qtde_avariada => "2", :data_recebimento => "08/08/2013", :usuario => "JOÃO"}}
		let(:other_valid_receipt_cimento) {{:pre_receipt => PreReceipt.create!(other_pre_receipt_cimento), :rastreio => "10", :avarias => "0", :qtde_avariada => "0", :data_recebimento => "08/08/2013"}}
		let(:valid_receipt_tijolo) {{:pre_receipt => PreReceipt.create!(valid_pre_receipt_tijolo), :rastreio => "101", :avarias => "1", :qtde_avariada => "5", :data_recebimento => "08/08/2013"}}
		
		before do
			@product_cimento = Product.create! cimento
			@product_tijolo = Product.create! tijolo
		end

		describe "locate" do
			before do
				@receipt_cimento = Receipt.create! valid_receipt_cimento
				@other_receipt_cimento = Receipt.create! other_valid_receipt_cimento
				@receipt_tijolo = Receipt.create! valid_receipt_tijolo
			end			
			it "returns all receipts when no filters given" do
				@receipts = Receipt.locate({})
				expect(@receipts.length).to be(3)
				expect(@receipts[0]).to eq(@receipt_cimento)
				expect(@receipts[1]).to eq(@other_receipt_cimento)
				expect(@receipts[2]).to eq(@receipt_tijolo)
			end
			it "filters receipts by product" do
				@receipts = Receipt.locate({product_id: 1})
				expect(@receipts.length).to be(2)
				expect(@receipts[0]).to eq(@receipt_cimento)
				expect(@receipts[1]).to eq(@other_receipt_cimento)
			end
			it "filters receipts by product_type" do
				@receipts = Receipt.locate({product_type_id: 2})
				expect(@receipts.length).to be(1)
				expect(@receipts[0]).to eq(@receipt_tijolo)
			end	
			it "filters receipts by tracking" do
				@receipts = Receipt.locate({rastreio: "10"})
				expect(@receipts.length).to be(2)
				expect(@receipts[0]).to eq(@receipt_cimento)
				expect(@receipts[1]).to eq(@other_receipt_cimento)
			end
			it "filters receipts by amount" do
				@receipts = Receipt.locate({quantidade: "20"})
				expect(@receipts.length).to be(1)
				expect(@receipts[0]).to eq(@receipt_tijolo)
			end
			it "filters receipts by breakdown" do
				@receipts = Receipt.locate({avarias: "1"})
				expect(@receipts.length).to be(2)
				expect(@receipts[0]).to eq(@receipt_cimento)
				expect(@receipts[1]).to eq(@receipt_tijolo)
			end
			it "filters receipts by address" do
				@receipts = Receipt.locate({address_id: "2"})
				expect(@receipts.length).to be(1)				
				expect(@receipts[0]).to eq(@receipt_tijolo)
			end
			it "filters receipts by user" do
				@receipts = Receipt.locate({usuario: "João"})
				expect(@receipts.length).to be(1)				
				expect(@receipts[0]).to eq(@receipt_cimento)
			end
		end
end
