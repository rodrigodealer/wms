require 'spec_helper'

describe FileStorage do
  describe "Validations" do
    it { should validate_presence_of :name }
    
    it { should validate_attachment_presence(:file) }
    it { should have_attached_file(:file) }
  end
  
end
