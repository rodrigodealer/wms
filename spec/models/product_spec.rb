# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Product do
  describe "Validations" do
    it { should validate_presence_of :name }
    it { should validate_presence_of :codigo }
    it { should validate_presence_of :provider }
  end
  
  describe "Associations" do
    it { should belong_to(:provider) }
    it { should belong_to(:product_type) }
  end
  
  describe "Volume and area" do
    let(:product) { Product.new(:altura => 10, :largura => 3, :comprimento => 2)}
    
    it "should calculate volume for product equal 60" do
      volume = product.volume
      expect(volume).to be_eql(60.0)
    end
    
    it "should calculate area for product equal 6" do
      area = product.area
      expect(area).to be_eql(6.0)
    end
  end
  context "products report" 
    
    let(:valid_cimento_address) {{id: 1, nome: "Endereco dos cimentos"}}
    let(:valid_tijolo_address) {{id: 2, nome: "Endereco dos tijolos"}}
    let(:cimento) {{:id => 1, :name => "Cimento", :codigo => '3556', :product_type_id => "1", :provider => Provider.new(:id => 1)}}
    let(:tijolo) {{:id => 2, :name => "Tijolo", :codigo => '3557', :product_type_id => "2", :provider => Provider.new(:id => 2)}}

    before do
      @product_cimento = Product.create! cimento
      @product_tijolo = Product.create! tijolo
    end    

    describe "#locate" do
      it "returns all products when no filters" do
        product = Product.new
        products = product.locate({})
        expect(products[0]).to eq(@product_cimento)
        expect(products[1]).to eq(@product_tijolo)
      end
      it "filters products by name" do
        product = Product.new
        products = product.locate({:name => 'Cimento'})
        expect(1).to eq(products.length)
        expect(products[0]).to eq(@product_cimento)
      end
      it "filters products by type" do
        product = Product.new
        products = product.locate({:product_type_id => '2'})
        expect(1).to eq(products.length)
        expect(products[0]).to eq(@product_tijolo)
      end
      it "filters products by prodiver" do
        product = Product.new
        products = product.locate({:provider_id => '1'})
        expect(1).to eq(products.length)
        expect(products[0]).to eq(@product_cimento)
      end
      it "filters products by code" do
        product = Product.new
        products = product.locate({:codigo => '3556'})
        expect(1).to eq(products.length)
        expect(products[0]).to eq(@product_cimento)
      end
    end
end
