# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Provider do
  describe "Validations" do
    it { should validate_presence_of :nome }
    it { should_not validate_presence_of :endereco }
    it { should_not validate_presence_of :cidade }
    it { should_not validate_presence_of :pais }
    it { should_not validate_presence_of :email }
    it { should_not validate_presence_of :contato }
    it { should_not validate_presence_of :telefone }
    it { should_not allow_value("blah").for(:email) }
    it { should allow_value("a@b.com").for(:email) }
    
    describe "Email validation" do
      let(:attributes) { FactoryGirl.attributes_for(:provider)}
      
      it "is valid" do
        provider = Provider.new(attributes)
        expect(provider).to be_valid
      end
      
      it "is invalid" do
        provider = Provider.new(attributes)
        provider.email = "a"
        expect(provider).to be_invalid
      end
    end
  end
  
end
