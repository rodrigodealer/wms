# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PreReceipt do
	describe "Validations" do
		it { should validate_presence_of :rastreio }
		it { should validate_presence_of :quantidade }
		it { should validate_presence_of :fabricacao }
		it { should validate_presence_of :embarque }
		it { should validate_presence_of :packing }		
		it { should validate_presence_of :container }
		it { should validate_presence_of :status }		
		it { should validate_presence_of :address_id }		
	end

	describe "Associations" do
		it { should belong_to :product }
		it { should belong_to :address }
	end	
  
	context "product code is valid" do
		describe "#gera_rastreio" do
			it "should generate tracking code" do							
				Random.should_receive(:rand).with(any_args()).and_return(763)
				pre_receipt = PreReceipt.new :packing => 'A421'				
				pre_receipt.gera_rastreio! '35'
				expect(pre_receipt.rastreio).to eq('A421-35-0763')
			end
		end
	end
    
	context "product code is invalid" do
		describe "#gera_rastreio" do
			it "should generate default tracking code" do
				pre_receipt = PreReceipt.new :packing => 'A421'
				expect {
					pre_receipt.gera_rastreio! nil
				}.to raise_error(ArgumentError, ":codigo must be valid")
			end
		end
  	end
end