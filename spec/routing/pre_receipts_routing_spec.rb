# -*- encoding : utf-8 -*-
require "spec_helper"

describe PreReceiptsController do 

	describe "routing" do
		it "routes to #index" do
			get("/pre_receipts").should route_to("pre_receipts#index")
		end

		it "routes to #show" do
			get("/pre_receipts/1").should route_to("pre_receipts#show", :id => "1")
		end

		it "routes to #new" do
			get("/pre_receipts/new").should route_to("pre_receipts#new")
		end

		it "routes to #edit" do
		  get("/pre_receipts/1/edit").should route_to("pre_receipts#edit", :id => "1")
		end

		it "routes to #create" do
		  post("/pre_receipts").should route_to("pre_receipts#create")
		end

		it "routes to #update" do
		  put("/pre_receipts/1").should route_to("pre_receipts#update", :id => "1")
		end

		it "routes to #destroy" do
		  delete("/pre_receipts/1").should route_to("pre_receipts#destroy", :id => "1")
		end

	end

end