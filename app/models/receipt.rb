class Receipt < ActiveRecord::Base
  resourcify
  
  validates :rastreio, :data_recebimento, :pre_receipt, :presence => true
  
  belongs_to :pre_receipt

  def self.locate(filtros)
  	@receipts = Receipt.includes({pre_receipt: [:product]}).references({pre_receipt: [:product]})    
  	if filter_valid?(filtros[:product_id])
  		@receipts = @receipts.where('pre_receipts.product_id = ?', filtros[:product_id])
  	end
  	if filter_valid?(filtros[:product_type_id])
  		@receipts = @receipts.where('products.product_type_id = ?', filtros[:product_type_id])
  	end
  	if filter_not_null_or_empty?(filtros[:rastreio])
  		@receipts = @receipts.where('receipts.rastreio = ?', filtros[:rastreio])
  	end
  	if filter_valid?(filtros[:quantidade])
  		@receipts = @receipts.where('pre_receipts.quantidade = ?', filtros[:quantidade])
  	end
  	if filter_valid?(filtros[:avarias])
  		@receipts = @receipts.where('receipts.avarias = ?', filtros[:avarias])
  	end
    if filter_valid?(filtros[:address_id])
      @receipts = @receipts.where('pre_receipts.address_id = ?', filtros[:address_id])
    end
    if filter_not_null_or_empty?(filtros[:usuario])
      @receipts = @receipts.where('UPPER(receipts.usuario) = ?', filtros[:usuario].capitalize)
    end
  	@receipts
  end
  
  private
    def self.filter_valid?(filter)
      !filter.nil? && filter.to_i > 0
    end
    
    def self.filter_not_null_or_empty?(filter)
      !filter.nil? && !filter.empty?
    end

end
