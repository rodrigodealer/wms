# -*- encoding : utf-8 -*-
class Product < ActiveRecord::Base
  resourcify
  
  belongs_to :provider
  belongs_to :product_type
  
  validates :name, :codigo, :provider, :product_type_id, :presence => true
  
  def volume
    altura  * largura * comprimento
  end
  
  def area
    largura * comprimento
  end

  def locate(filtros)
    @products = Product.all  
    if (!filtros[:name].nil? && filtros[:name] != '')       
      @products = @products.where('name = ?', filtros[:name])
    end
    if (!filtros[:product_type_id].nil? && filtros[:product_type_id].to_i > 0)       
      @products = @products.where('product_type_id = ?', filtros[:product_type_id])
    end
    if (!filtros[:provider_id].nil? && filtros[:provider_id].to_i > 0)       
      @products = @products.where('provider_id = ?', filtros[:provider_id])
    end
    if (!filtros[:codigo].nil? && filtros[:codigo] != '')       
      @products = @products.where('codigo = ?', filtros[:codigo])
    end
    @products
  end

end
