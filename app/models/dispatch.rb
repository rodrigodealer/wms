class Dispatch < ActiveRecord::Base

	belongs_to :product
	belongs_to :address

	validates :expedicao, :quantidade, :solicitante, :conferente, :conferente_adicional, :status, :presence => true

	def saida!
		if valida_armazenamento
			self.save
		else
			raise(RuntimeError, "produto nao disponivel em estoque neste endereco.")
		end
	end

	def valida_armazenamento
		valid = true		
		storage = Storage.new
		storages = storage.locate({data: self.expedicao.to_s(:date), product_id: self.product_id, address_id: self.address_id }) 
		if (!storages.nil?)
			product_storage = storages[0]
			if (product_storage.respond_to? 'storage')
				if (product_storage.storage < self.quantidade)
					valid = false
				end
			else 
				valid = false				
			end
		end
		valid
	end

end
