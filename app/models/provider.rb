# -*- encoding : utf-8 -*-
class Provider < ActiveRecord::Base
  resourcify
  
  validates :nome, :presence => true
  validates :email, :email => true
end

