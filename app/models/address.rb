# -*- encoding : utf-8 -*-
class Address < ActiveRecord::Base
  resourcify
  
  belongs_to :address
  has_many :addresses, dependent: :destroy
  
  validates :nome, :presence => true

  has_attached_file :file, :default_url => "/images/:style/missing.png"
  
  def parent_addresses(address = self, count = 0)
    return count if !has_address?(address)
    if has_address?(address)
      count += 1
      parent_addresses(address.address, count)
    end
  end
  
  private
    def has_address?(address)
      !address.address.nil?
    end
end
