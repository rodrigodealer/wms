class FileStorage < ActiveRecord::Base
  resourcify
  
  validates :name, :file, :presence => true
  
  has_attached_file :file, :default_url => "/images/:style/missing.png"
end
