class Storage
  include ActiveModel::Model
	attr_accessor :product, :receipt, :dispatch, :storage, :history_receipts, :history_dispatches

	def locate(filtros)	
    	date = date_format(filtros[:data])
		pre_receipts = filtra_pre_receipts(filtros, date)
		dispatches = filtra_dispatches(filtros, date)
		calcula_armazenamento(pre_receipts, dispatches, date)	
	end

	def diferenca
		self.storage = receipt - dispatch
	end

	private		

		def filtra_dispatches(filtros, date)
			dispatches = Dispatch.includes([:product, :address]).references([:product, :address]).where('expedicao <= ?', date)			
			if filter_valid?(filtros[:product_type_id])
				dispatches = dispatches.where("products.product_type_id = ?", filtros[:product_type_id])
			end
			if filter_valid?(filtros[:product_id])
				dispatches = dispatches.where("products.id = ?", filtros[:product_id])
			end
			if filter_valid?(filtros[:address_id])
				dispatches = dispatches.where("addresses.id = ?", filtros[:address_id])
			end
			dispatches
		end

		def filtra_pre_receipts(filtros, date)
			pre_receipts = PreReceipt.includes([:product, :receipt, :address]).references([:product, :receipt, :address]).where('receipts.data_recebimento <= ? and receipts.id is not null', date)			
			if filter_valid?(filtros[:product_type_id])
				pre_receipts = pre_receipts.where("products.product_type_id = ?", filtros[:product_type_id])
			end
			if filter_valid?(filtros[:product_id])
				pre_receipts = pre_receipts.where("products.id = ?", filtros[:product_id])
			end
			if filter_valid?(filtros[:address_id])
				pre_receipts = pre_receipts.where("addresses.id = ?", filtros[:address_id])
			end
			pre_receipts
		end
    
    def filter_valid?(filter)
      !filter.nil? && filter.to_i > 0
    end

    def date_format(date)
      date.split("/").reverse.join("-")
    end
  
		def initialize_storage(product, receipt, dispatch)
			storage = Storage.new
			storage.product = product
			storage.receipt = receipt
			storage.dispatch = dispatch
			storage.history_receipts = Array.new
			storage.history_dispatches = Array.new			
			storage
		end

		def create_receipt_history(item)
			receipt_history = HistoryReceipt.new
			receipt_history.pre_receipt = item
			receipt_history.receipt = item.receipt			
			receipt_history
		end
    	    	
    def preenche_dispatch_or_pre_receipt(itens, storages, type)
			itens.each {|item|
				product_index = procura_produto(item.product_id, storages)
				unless product_index.nil? 
					storage = storages[product_index]					
					storage.receipt = storage.receipt.+(item.quantidade.to_i - item.receipt.qtde_avariada.to_i) if type == Receipt
			        storage.dispatch = storage.dispatch.+(item.quantidade.to_i) if type == Dispatch				        
			        if type == Receipt
			        	receipt_history = create_receipt_history(item)
			        	storage.history_receipts << receipt_history
			        end				        					        				        
			        storage.history_dispatches << item if type == Dispatch
					storage.diferenca
					storages[product_index] = storage					
				else
					storage = initialize_storage(item.product, item.quantidade.to_i - item.receipt.qtde_avariada.to_i, 0) if type == Receipt
         			storage = initialize_storage(item.product, 0, item.quantidade.to_i) if type == Dispatch
         			if type == Receipt
			        	receipt_history = create_receipt_history(item)
			        	storage.history_receipts << receipt_history
			        end	
         			storage.history_dispatches << item if type == Dispatch
					storage.diferenca
					storages << storage
				end
			}
			storages
    end

		def calcula_armazenamento(pre_receipts, dispatches, data)
			storages = Array.new
			storages = preenche_dispatch_or_pre_receipt(pre_receipts, storages, Receipt)			
			storages = preenche_dispatch_or_pre_receipt(dispatches, storages, Dispatch)
			storages
		end		
		
		def procura_produto(id, storages)			
			product_index = nil
			storages.each_with_index { |storage, index|
				if id == storage.product.id 
					product_index = index
					break
				end
      		}
			product_index
		end
end