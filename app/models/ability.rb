class Ability
  include CanCan::Ability

  def initialize(user)
    
    if user.has_role? :admin
      can :manage, :all
    else
      # can :manage, [:saida_estoque, :entrada_estoque]
      cannot :read, [ Address, Product, Provider, User ]
    end
  end
end
