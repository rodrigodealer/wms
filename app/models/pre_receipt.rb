# -*- encoding : utf-8 -*-
class PreReceipt < ActiveRecord::Base
  belongs_to :address
  belongs_to :product
  has_one :receipt

  validates :rastreio, :quantidade, :fabricacao, :embarque, :packing, :container, :status, :address_id, :product, :presence => true

  def gera_rastreio!(codigo)
  	if !codigo.nil?
      random_number = Random.rand(1..1000)
  		self.rastreio = packing + '-' + codigo + '-' + ('%.4d' % random_number)
  	else
  		raise(ArgumentError, ":codigo must be valid")
  	end
  end

end