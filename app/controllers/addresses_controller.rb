# -*- encoding : utf-8 -*-
class AddressesController < ApplicationController
  load_and_authorize_resource
  before_action :set_address, only: [:show, :edit, :update, :destroy]

  def index
    @addresses = Address.all
  end

  def show
  end

  def new
    @address = Address.new
    @addresses = Address.all
  end

  def edit
    @addresses = Address.all
  end

  def create
    @address = Address.new(address_params)
    if @address.save
      redirect_to @address, notice: 'Endereço salvo com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    if @address.update(address_params)
      redirect_to @address, notice: 'Endereço salvo com sucesso.'
    else
      render action: 'edit'
    end
  end

  def destroy
    if @address.destroy
      redirect_to addresses_url
    end
  end

  private
    def set_address
      @address = Address.find(params[:id])
    end

    def address_params
      params.require(:address).permit(:nome, :address_id, :file)
    end
end
