# -*- encoding : utf-8 -*-
class ProductsController < ApplicationController
  load_and_authorize_resource
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all
  end

  def show
  end

  def new
    @product = Product.new
    @providers = Provider.all
    @product_types = ProductType.all
  end

  def edit
    @providers = Provider.all
    @product_types = ProductType.all
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product, notice: 'Produto salvo com sucesso.'
    else
      @providers = Provider.all
      @product_types = ProductType.all
      render action: 'new'
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Produto salvo com sucesso.'
    else
      @providers = Provider.all
      @product_types = ProductType.all
      render action: 'edit'
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:codigo, :name, :provider_id, :peso, :altura, :largura, :comprimento, :product_type_id)
    end
end
