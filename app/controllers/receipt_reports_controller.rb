class ReceiptReportsController < ApplicationController
	authorize_resource :class => false

	skip_before_filter :verify_authenticity_token, :only => [:report]	
	respond_to :json, :html, :pdf
	

	def index
		@product_types = ProductType.all
		@products = Product.all
		@addresses = Address.all.to_set.classify {
			|address| address.address
		}
	end

	def report		
		@receipts = Receipt.locate(params)
		respond_with(@receipts, :responder => CallResponder)
	end

end
