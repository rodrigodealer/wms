class ProductReportsController < ApplicationController
	authorize_resource :class => false
	
	skip_before_filter :verify_authenticity_token, :only => [:report]	
	respond_to :json, :html, :pdf

	def index    
    	@product_types = ProductType.all    
    	@providers = Provider.all
  	end

	def report
		product = Product.new
		@products = product.locate(params)
		respond_with(@products, :responder => CallResponder)
	end

end
