class ProductTypesController < ApplicationController

	load_and_authorize_resource

	before_action :set_product_type, only: [:edit, :update, :destroy]

	def index
		@product_types = ProductType.all
	end

	def new
		@product_type = ProductType.new
	end

	def edit
	end

	def create
		@product_type = ProductType.new(product_type_params)
		if @product_type.save
			redirect_to product_types_path, notice: 'Tipo de produto salvo com sucesso.'
		else
			render action: "new"
		end
	end

	def update
		if @product_type.update(product_type_params)
			redirect_to product_types_path, notice: 'Tipo de produto alterado com sucesso.'
		else
			render action: "edit"
		end
	end

	def destroy
		@product_type.destroy
		redirect_to product_types_path
	end

	private
  	def set_product_type
      @product_type = ProductType.find(params[:id])
  	end

  	def product_type_params
    		params.require(:product_type).permit(:descricao)
  	end
end
