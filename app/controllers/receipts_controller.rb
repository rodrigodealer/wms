# -*- encoding : utf-8 -*-
class ReceiptsController < ApplicationController
  load_and_authorize_resource
  
  def index
    @pre_receipts = PreReceipt.includes(:receipt).references(:receipt)
  end

  def new
    @pre_receipt = PreReceipt.find(params[:id])
    @products = Product.all
    @addresses = Address.all
    @receipt = Receipt.new
  end

  def edit
    @receipt = Receipt.find(params[:id])
    @products = Product.all
    @addresses = Address.all
    @pre_receipt = @receipt.pre_receipt
  end
  
  def create
    @receipt = Receipt.new(receipt_params)
    pre_receipt = PreReceipt.find(params[:receipt][:pre_receipt_id])
    @receipt.pre_receipt = pre_receipt    
		if @receipt.save				
			redirect_to receipts_path, notice: 'Recebimento salvo com sucesso.'
		else
			render action: 'new'
		end			
  end
  
  def update
    @receipt = Receipt.find(params[:id])
    if @receipt.update(receipt_params)
      redirect_to receipts_path, notice: 'Recebimento salvo com sucesso.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @receipt = Receipt.find(params[:id])    
    if @receipt.destroy
      redirect_to receipts_path, notice: 'Recebimento excluído com sucesso.'
    else
      redirect_to receipts_path, notice: 'Erro ao excluir recebimento.'
    end
  end
  
	private 	
		def receipt_params
			params.require(:receipt).permit(:rastreio, :data_recebimento, :avarias, :qtde_avariada, :descricao_avarias, 
                                      :pre_receipt_id, :placa_caminhao, :usuario)
		end
end
