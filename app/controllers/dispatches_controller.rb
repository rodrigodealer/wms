# -*- encoding : utf-8 -*-
class DispatchesController < ApplicationController
	load_and_authorize_resource
	before_action :set_dispatch, only: [:edit, :update]
	before_action :set_products, only: [:new, :edit]	
	before_action :set_addresses, only: [:new, :edit]

	def index
		@dispatches = Dispatch.all
	end

	def new		
	end		

	def edit
	end

	def create
		@dispatch = Dispatch.new(dispatch_params)
		respond_to do |format|
			begin				
				if @dispatch.saida!				
					format.js { render :json => @dispatch.to_json(:include => { :product => {:only => :name} } ) }
				else
					format.js { render json: @dispatch.errors, status: :unprocessable_entity }
				end
			rescue RuntimeError => e
				format.js { render :json => {error: "#{e}"}}
			end
		end		
	end

	def update		
		if @dispatch.update(dispatch_params)
			redirect_to dispatches_path, notice: 'Expedição alterada com sucesso.'
		else
			render action: 'edit'
		end
	end

	private
		def set_products
			@products = Product.all
		end

		def set_dispatch
			@dispatch = Dispatch.find params[:id]
		end

		def set_addresses			
			@addresses = Address.all.to_set.classify {
				|address| address.address
			}	
		end

		def dispatch_params
			params.require(:dispatch).permit(:id, :product_id, :quantidade, :expedicao, :solicitante, 
				:conferente, :conferente_adicional, :status, :address_id)
		end

end
