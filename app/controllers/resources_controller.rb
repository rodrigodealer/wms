# -*- encoding : utf-8 -*-
class ResourcesController < ApplicationController
  LANGUAGE_DIR="app/assets/language"
  
  def language
    file_path = "#{LANGUAGE_DIR}/#{params[:language]}.txt"
    send_file file_path
  end
  
end
