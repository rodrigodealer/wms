class StoragesController < ApplicationController
	skip_before_filter :verify_authenticity_token, :only => [:report]	
	respond_to :json, :html, :pdf
	load_and_authorize_resource	
	
	def index
		@addresses = Address.all.to_set.classify {
			|address| address.address
		}
		@product_types = ProductType.all	
		@products = Product.all	
	end

	def locate				
		storage = Storage.new		
		@storages = storage.locate(params)
		respond_to do |format|
			format.json { render :json => @storages.to_json}			
		end		
	end

	def report
		storage = Storage.new		
		@storages = storage.locate(params)
		respond_with(@storages, :responder => CallResponder)
	end

end
