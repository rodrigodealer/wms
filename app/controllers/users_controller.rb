# -*- encoding : utf-8 -*-
class UsersController < ApplicationController
  load_and_authorize_resource
  respond_to :json, :html, :pdf
  
  def index
    @users = User.all
    respond_with(:users => @users)
  end
  
  def new
    @user = User.new
    @roles = Role.all
  end
  
  def edit
    @user = User.find(params[:id])
    @roles = Role.all
    respond_with(@user)
  end
  
  def save
    set_user
    if (params[:user][:id] == "")      
      if @user.save
        redirect_to action: 'index'
      else
        render action: 'new'
      end
    else
      if @user.update(user_params)
        redirect_to action: 'index'
      else
        render action: 'new'
      end
    end
  end
  
  def roles
    @roles = User.find(params[:id]).roles.select("id, name")
    respond_with(@roles)
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path }
    end
  end
  
  def show
  end
  
  def report
    @users = User.all
    respond_with(@users, :responder => CallResponder)
  end
  
  private
    def split_roles_and_process
      if !params[:roles].empty?
        params[:roles].split(",").reject!(&:blank?).each {|r|
          role = Role.find(r)
          @user.add_role(role.name.to_sym)
        }
      end
    end
  
    def set_user
      if params[:user][:id] == ""
        @user = User.new(user_params)
        split_roles_and_process
      else
        @user = User.find_by_email(params[:user][:email])
        split_roles_and_process
      end
    end

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :id)
    end
end
