class PreReceiptsController < ApplicationController
	load_and_authorize_resource
	before_action :set_pre_receipt, only: [:show, :edit, :update, :destroy]
	skip_before_filter :verify_authenticity_token, :only => [:create, :destroy]

	def index
		@pre_receipts = PreReceipt.all
	end

	def show
	end

	def new
		@pre_receipt = PreReceipt.new
		@addresses = Address.all.to_set.classify {
			|address| address.address
		}
		@products = Product.all
	end

	def edit
		@addresses = Address.all
		@products = Product.all
	end

	def create
		@pre_receipt = PreReceipt.new(pre_receipt_params)
		product = Product.find(@pre_receipt.product_id)
		@pre_receipt.gera_rastreio!(product.codigo)
		respond_to do |format|
			if @pre_receipt.save				
				format.html { redirect_to @pre_receipt, notice: 'Pre Recebimento cadastrado com sucesso.'}
				format.json { render :json => @pre_receipt.to_json(:include => {:product => { :only => :name }, :address => { :only => :nome }}) }
			else
				format.html { render action: 'new' }				
				format.json { render json: @pre_receipt.errors, status: :unprocessable_entity } 
			end			
		end
	end

	def update
		if @pre_receipt.update(pre_receipt_params)
			redirect_to @pre_receipt, notice: 'Pre Recebimento salvo com sucesso.'
		else
			render action: 'edit'
		end
	end

	def destroy
		respond_to do |format|
			if @pre_receipt.destroy
				format.html { redirect_to pre_receipts_url }
				format.json { render json: {status: 'ok'} }
			end
		end				
	end

	private 	
		def set_pre_receipt
			@pre_receipt = PreReceipt.find params[:id]
		end

		def pre_receipt_params
			params.require(:pre_receipt).permit(:id, :product_id, :address_id, :quantidade, :fabricacao, :embarque, :packing, :container, :status, :navio)
		end

end