class FileStoragesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_file_storages, only: [:show, :edit, :update, :destroy]
  
  def index
    @file_storages = FileStorage.all
  end

  def show
  end

  def new
    @file_storage = FileStorage.new
  end

  def edit
  end

  def create
    @file_storage = FileStorage.new(file_storage_params)
    if @file_storage.save
      redirect_to @file_storage, notice: 'Arquivo salvo com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    if @file_storage.update(file_storage_params)
      redirect_to @file_storage, notice: 'Arquivo salvo com sucesso.'
    else
      render action: 'edit'
    end
  end

  def destroy
    if @file_storage.destroy
      redirect_to file_storages_path
    end
  end

  private
    def set_file_storages
      @file_storage = FileStorage.find(params[:id])
    end

    def file_storage_params
      params.require(:file_storage).permit(:name, :file)
    end
end
