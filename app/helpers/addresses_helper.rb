# -*- encoding : utf-8 -*-
module AddressesHelper
  def address_name_increment(address)
    increment = ""
    address.parent_addresses().times do
      increment << "*"
    end
    "#{increment} #{address.nome}"
  end
  
  def show_if_has_address(address)
    address.address.nome if address.address
  end

end
