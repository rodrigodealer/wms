# -*- encoding : utf-8 -*-
module UsersHelper
  
  def roles_to_view(roles)
    roles.map {|r| r.name }.join(", ")
  end
end
