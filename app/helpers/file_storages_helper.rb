module FileStoragesHelper
  
  def format_created_at(created_at)
    created_at.strftime("%m/%d/%Y %I:%M")
  end
end
