module ReportHelper
  
  APPLICATION_CSS = "app/assets/stylesheets/application.css"
  
  def all_css_files
    css_files = Dir["app/assets/stylesheets/*.css"]
    css_erb_files = Dir["app/assets/stylesheets/*.css*erb"]
    (css_files + css_erb_files).reject { |f| f == APPLICATION_CSS }
  end
  
  def read_content(files)
    contents = ""
    files.each {|f|
      file = File.open(f)
      file.each {|line| contents << line }
    }
    contents
  end
  
  def all_css_content_with_tags
    content = read_content(all_css_files)
    raw("<style>#{content}</style>")
  end
  
end
