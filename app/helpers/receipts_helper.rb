module ReceiptsHelper
  
  def receipt_link(pre_receipt)
    if pre_receipt.receipt
      edit_receipt_path(pre_receipt.receipt.id)
    else
      new_receipt_path(pre_receipt.id)
    end
  end
  
  def receipt_link_text(pre_receipt)
    if pre_receipt.receipt
      "Editar recebimento"
    else
      "Criar recebimento"
    end
  end
end
