json.array!(@products) do |product|
  json.extract! product, :name, :provider_id, :peso, :tipo, :altura, :largura, :comprimento
  json.url product_url(product, format: :json)
end
