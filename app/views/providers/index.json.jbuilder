json.array!(@providers) do |provider|
  json.extract! provider, :nome, :endereco, :cidade, :estado
  json.url provider_url(provider, format: :json)
end
