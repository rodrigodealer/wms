$(document).ready(function(){

	var date = new Date();
	var weekdays = "Domingo, Segunda-feira, Terça-feira, Quarta-feira, Quinta-feira, Sexta-feira, Sábado".split(", ");
	month = date.getMonth() + 1

	$("#cab_topo_data").text(date.getDate() + "/" + month  + "/" + date.getFullYear() + ", " + weekdays[date.getDay()] );



	$(window).resize(function()
	{
		if($(window).width() >= 765){
			$(".sidebar .sidebar-inner").slideDown(350);
		}
		else{
			$(".sidebar .sidebar-inner").slideUp(350); 
		}
	});

	$('#reportProviders').dataTable({
		"bRetrieve": true,
		"sDom": "<'row-fluid dataTables_filter_qtd'<'span5'l><'pull-right'f>r>t<'widget-foot'<'span6 pagination'i><'pagination pull-right'p><'clearfix'>",
		"oLanguage": {
			"sUrl": "resources/language/pt_BR.txt"
		},
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 4, 5 ] }
		]
	});

	$('#reportProducts').dataTable({
		"bRetrieve": true,
		"sDom": "<'row-fluid dataTables_filter_qtd'<'span5'l><'pull-right'f>r>t<'widget-foot'<'span6 pagination'i><'pagination pull-right'p><'clearfix'>",
		"oLanguage": {
			"sUrl": "resources/language/pt_BR.txt"
		},
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 7, 8 ] }
		]
	});

	$('#reportAddress').dataTable({
		"bRetrieve": true,
		"sDom": "<'row-fluid dataTables_filter_qtd'<'span5'l><'pull-right'f>r>t<'widget-foot'<'span6 pagination'i><'pagination pull-right'p><'clearfix'>",
		"oLanguage": {
			"sUrl": "resources/language/pt_BR.txt"
		},
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 2, 3] }
		]
	});

	$(".select-2").select2({
		allowClear: true,
		placeholder: "Selecione um item"
	});

});

$(document).ready(function(){

	$(".has_submenu > a").click(function(e){
		e.preventDefault();
		var menu_li = $(this).parent("li");
		var menu_ul = $(this).next("ul");

		if(menu_li.hasClass("open")){
			menu_ul.slideUp(350);
			menu_li.removeClass("open")
		}
		else{
			$(".navi > li > ul").slideUp(350);
			$(".navi > li").removeClass("open");
			menu_ul.slideDown(350);
			menu_li.addClass("open");
		}
	});

});

$(document).ready(function(){
	$(".sidebar-dropdown a").on('click',function(e){
		e.preventDefault();

		if(!$(this).hasClass("dropy")) {
		// hide any open menus and remove all other classes
		$(".sidebar .sidebar-inner").slideUp(350);
		$(".sidebar-dropdown a").removeClass("dropy");
		
		// open our new menu and add the dropy class
		$(".sidebar .sidebar-inner").slideDown(350);
		$(this).addClass("dropy");
	}
	
	else if($(this).hasClass("dropy")) {
		$(this).removeClass("dropy");
		$(".sidebar .sidebar-inner").slideUp(350);
	}
});

});

$('.wclose').click(function(e){
	e.preventDefault();
	var $wbox = $(this).parent().parent().parent();
	$wbox.hide(100);
});

$('.wminimize').click(function(e){
	e.preventDefault();
	var $wcontent = $(this).parent().parent().next('.widget-content');
	if($wcontent.is(':visible')) 
	{
		$(this).children('i').removeClass('icon-chevron-up');
		$(this).children('i').addClass('icon-chevron-down');
	}
	else 
	{
		$(this).children('i').removeClass('icon-chevron-down');
		$(this).children('i').addClass('icon-chevron-up');
	}            
	$wcontent.toggle(500);
}); 




$(document).ready(function(){
	$("#slist a").click(function(e){
		e.preventDefault();
		$(this).next('p').toggle(200);
	});
});