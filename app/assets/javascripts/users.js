
var UserCtrl = ['$scope', '$http', function UserCtrl($scope, $http) {
	$scope.profiles = [];
	$scope.selectedRoles = "";
	$scope.selectedProfile = "";
	
	$scope.init = function(id) {
		if (id) {
			$http.get(id + '/roles.json').success(function(data) {
				$scope.profiles = data;
				$scope.reloadProfiles();
			});
		}
	}
  
	$scope.addProfile = function() {
		var id = $scope.selectedProfile;
		if (!$scope.hasProfile(id)) {
			$scope.profiles.push(
				{
					"name": $("option[value='" + id + "']").text(),
					"id": id
				}
			);
			$scope.reloadProfiles();
		}
	};
	
	$scope.reloadProfiles = function() {
		$scope.selectedRoles = "";
		$.each($scope.profiles, function(i, profile) {
			$scope.selectedRoles = $scope.selectedRoles + "," + profile.id;
		});
	};
	
	$scope.removeProfile = function(id) {
		$.each($scope.profiles, function(i, profile) {
			if (profile.id === id) {
				$scope.profiles.splice(i, 1);
			}
		});
		$scope.reloadProfiles();
	}
	
	$scope.hasProfile = function(id) {
		var hasProfile = false;
		$.each($scope.profiles, function(i, profile) {
			if (profile.id === id) {
				hasProfile = true;
			}
		});
		return hasProfile;
	}
}];
