var StoragesController = ['$scope', '$http', function($scope, $http){	

	$scope.storages = [];

	$scope.init = function() {		
	};

	$scope.reloadStorages = function(storages){
		$scope.storages = [];
		for (var i = 0; i < storages.length; i++) {
			storage = storages[i];
			storage = $scope.adjustHistoryDates(storage);
			$scope.storages.push(storage);
		};
	};

	$scope.adjustHistoryReceipts = function(history_receipts){
		for (var i = 0; i < history_receipts.length; i++){
			history = history_receipts[i];
			history.receipt.data_recebimento = new Date(history.receipt.data_recebimento).toString('dd/MM/yyyy');
			history_receipts[i] = history;
		}
		return history_receipts;
	}

	$scope.adjustHistoryDispatches = function(history_dispatches){
		for (var i = 0; i < history_dispatches.length; i++){
			history = history_dispatches[i];
			history.expedicao = new Date(history.expedicao).toString('dd/MM/yyyy');
			history_dispatches[i] = history;
		}
		return history_dispatches;
	};

	$scope.adjustHistoryDates = function(storage){		
		storage.history_receipts = $scope.adjustHistoryReceipts(storage.history_receipts);
		storage.history_dispatches = $scope.adjustHistoryDispatches(storage.history_dispatches);		
		return storage;
	};

	$scope.showDetails = function(){
		console.log(this);
	};

	$scope.recarregar = function(){
		if (typeof($scope.data) != 'undefined' && $scope.data != ''){
			$http({
			    url: 'storages/locate.json', 
			    method: "GET",
			    params: {
			    	data: $scope.data,
			    	address_id: $scope.address,
			    	product_type_id: $scope.product_type,
			    	product_id: $scope.product
			    }
	 		}).success(function(data){
	 			$scope.reloadStorages(data);
				//clearTable();
				//fillTable(data);
	 		});
		} else {
			alert('Preencha a data de pesquisa.');
		}
	};
}]

$(document).ready(function(){
	$("body").click(function(e){
		element = $(e.target);
		if (element.hasClass('detail')){
			showItems(element);
		}
	});
});

function showItems(element){				
	tableRow = element.parent().parent().next();
	tableRow.toggle('fast');	
	tableRow.siblings('.next').hide();	
};
