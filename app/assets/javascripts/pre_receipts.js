var reportPreReceipts;

var PreReceiptController = ['$scope', '$http', function PreReceiptController($scope, $http) {

	$scope.preReceipts = [];

	$scope.init = function() {

	};

	$scope.excluir = function(id){
		console.log('Excluindo');
		$http({
		    url: '/pre_receipts/' + id, 
		    method: "delete",
		    params: {	
		    	format: 'json'
		    }
 		}).success(function(data){
 			if (data.status == 'ok'){
 				$scope.removePreReceipt(id);
 			};
 		});
	};

	$scope.removePreReceipt = function(id){
		for (var i = 0; i < $scope.preReceipts.length; i++){
			var pre_receipt = $scope.preReceipts[i];
			if (pre_receipt.id === id){
				$scope.preReceipts.splice(i,1);
				break;
			}
		}
	};

	$scope.addPreReceipt = function(){
		$http({
		    url: '/pre_receipts', 
		    method: "post",
		    params: {
		    	"pre_receipt[product_id]": $scope.produto,
		    	"pre_receipt[address_id]": $scope.endereco,
		    	"pre_receipt[quantidade]": $scope.quantidade,
		    	"pre_receipt[fabricacao]": $scope.fabricacao,
		    	"pre_receipt[embarque]": $scope.embarque,
		    	"pre_receipt[packing]": $scope.packing,
		    	"pre_receipt[container]": $scope.container,
		    	"pre_receipt[navio]": $scope.navio,
		    	"pre_receipt[status]": 'Embarcado',
		    	format: 'json'
		    }
 		}).success(function(data){
 			$scope.clearPreReceipts();
			$scope.reloadPreReceipts(data);
 		});
	};

	$scope.clearPreReceipts = function(){
		$("select[name='pre_receipt[product_id]']").select2("val", "");
		$("select[name='pre_receipt[address_id]']").select2("val", "");
		$scope.quantidade = "";
		$scope.fabricacao = "";
		$scope.embarque = "";
		$scope.packing = "";
		$scope.container = "";
	};

	$scope.reloadPreReceipts = function(pre_receipt){		
		pre_receipt.fabricacao = new Date(pre_receipt.fabricacao).toString('dd/MM/yyyy');
		pre_receipt.embarque = new Date(pre_receipt.embarque).toString('dd/MM/yyyy');
		$scope.preReceipts.push(pre_receipt);
	};
}];