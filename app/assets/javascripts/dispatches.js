var reportDispatches;

var dispatches = {};

dispatches.iniciaDataTable = function(){
	reportDispatches = $("#reportDispatchesNew").dataTable({
		"sDom" : 't'
	});
};

dispatches.addRow = function(json){
	expedicao = new Date(json.expedicao).toString("dd/MM/yyyy");
	reportDispatches.fnAddData([
				json.product.name,
				expedicao,
				json.quantidade,
				json.solicitante,
				json.conferente,
				json.conferente_adicional,
				json.status
			]);
};

dispatches.clearFields = function() {
	$("select[name*='dispatch']").val("").change().focus();
	$("input[name*='dispatch']").val("");
	$("#dispatch_status").val("Saída");
};

var DispatchController = ['$scope', function($scope){

	$scope.init = function() {

		dispatches.iniciaDataTable();

		$('#new_dispatch').bind('ajax:complete', function(xhr, data, status){
			var json = JSON.parse(data.responseText)
			if (typeof(json.error) != 'undefined'){				
				alert(json.error);
			} else {
				dispatches.addRow(json);
				dispatches.clearFields();				
			}						
		});
	}

}];
