# -*- encoding : utf-8 -*-
require "bundler/capistrano"
# require "rvm/capistrano"
load 'deploy/assets'

set :application, "wms"
set :scm,             :git
set :repository,      "git@github.com:pordotom/wms.git"
set :branch,          "master"
set :migrate_target,  :current
set :ssh_options,     { :forward_agent => true }
set :rails_env,       "production"
set :deploy_to,       "/home/deploy/apps/wms"
set :normalize_asset_timestamps, false

set :default_environment, {
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}
set :user,            "deploy"
set :group,           "staff"
set :use_sudo,        false

set :port, 2299
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :location_homol, "198.199.91.129"
set :location_prod, "192.241.136.37"
role :web, location_homol, location_prod                         # Your HTTP server, Apache/etc
role :app, location_homol, location_prod                       # This may be the same as your `Web` server
role :db,  location_homol, location_prod, :primary => true # This is where Rails migrations will run

set :bundle_flags, "--deployment --quiet --binstubs --shebang ruby-local-exec"
set (:bundle_cmd) { "/home/deploy/.rbenv/shims/bundle" }
# set :rvm_ruby_string, '1.9.3@election'
# set :rvm_type, :user

set :unicorn_pid do
  "/home/deploy/shared/pids/unicorn.pid"
end

before "deploy:assets:precompile", "bundle:install"
after "deploy", "deploy:migrate"

namespace :deploy do
  task :start do
    top.unicorn.start
  end

  task :stop do
    top.unicorn.stop
  end

  task :restart do
    top.unicorn.reload
  end
end

after "deploy:update_code", "db:symlink"
before "deploy:assets:precompile", "db:symlink"
 
namespace :db do
  desc "Make symlink for database yaml" 
  task :symlink do
    run "ln -nfs #{release_path}/config/database.yml.sample #{release_path}/config/database.yml" 
  end
end

namespace :unicorn do
  desc "start unicorn server"
  task :start, :roles => :app do
    run "/etc/init.d/unicorn start"
  end

  desc "stop unicorn server"
  task :stop do
    run "/etc/init.d/unicorn stop"
  end

  desc "restart unicorn"
  task :restart do
    run "/etc/init.d/unicorn restart"
  end

  desc "reload unicorn (gracefully restart workers)"
  task :reload do
    run "/etc/init.d/unicorn upgrade"
  end

  desc "reconfigure unicorn (reload config and gracefully restart workers)"
  task :reconfigure, :roles => :app do
    run "kill -s HUP `cat #{unicorn_pid}`"
  end
end
