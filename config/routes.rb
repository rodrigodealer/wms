# -*- encoding : utf-8 -*-
Wms::Application.routes.draw do
  resources :addresses

  resources :products

  resources :providers

  resources :pre_receipts

  devise_for :users
  
  root to: 'home#index'
  
  get 'permission_denied' => "home#permission_denied"
  
  get 'resources/language/:language' => "resources#language"
  
  get 'users/new' => "users#new", as: "new_user"
  get 'users/report' => "users#report", as: "report_users"
  get 'users/:id' => "users#edit", as: "edit_user"
  delete 'users/:id/delete' => "users#destroy", as: "delete_user"
  post 'users/save' => "users#save"
  patch 'users/save' => "users#save"
  get 'users/:id/roles' => "users#roles"
  get 'users' => "users#index"
  
  resources :receipts, :except => :new do
    get "new" => :new, as: "new", on: :member
  end

  resources :dispatches

  resources :product_types

  get 'storages' => "storages#index", as: "storages"
  get 'storages/locate' => "storages#locate", as: "locate_storages"
  get 'storages/report' => "storages#report", as: "report_storages"

  post 'product_reports/report' => "product_reports#report", as: "report_product_reports"
  get 'product_reports' => "product_reports#index"

  get 'receipt_reports' => "receipt_reports#index"
  post 'receipt_reports/report' => "receipt_reports#report", as: "report_receipt_reports"
  
  resources :file_storages
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
